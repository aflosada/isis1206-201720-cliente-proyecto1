package model.logic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.data_structures.*;
import model.vo.*;

public class ParteB extends STSManager {

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371 * 1000; // Radious of the earth

		Double latDistance = toRad(lat2 - lat1);
		Double lonDistance = toRad(lon2 - lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	// 1B

	public DoubleLinkedList<VORoute> ITSrutasPorEmpresaParadas(String nomEmpresa, String Fecha) {

		DoubleLinkedList<VORoute> respuesta = new DoubleLinkedList<VORoute>();

		DoubleLinkedList<String> serviceIDs2 = new DoubleLinkedList<>();

		for (int i = 0; i <= getCalendarDates().Size(); i++) {
			try {
				if (getCalendarDates().get(i).giveInfo().getDate().equals(Fecha)
						&& getCalendarDates().get(i).giveInfo().getException_type() == 1) {
					serviceIDs2.agregarComienzo(getCalendarDates().get(i).giveInfo().getService_id() + "");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (int i = 0; i < getAgency().Size(); i++) {
			Node<VOAgency> agen = getAgency().getPrimero();
			VOAgency agencia = agen.giveInfo();
			if (agencia.getAgency_name().equals(nomEmpresa)) {

				DoubleLinkedList<VORoute> serviciosTemp = agencia.getListaRutas();
				Node<VORoute> strings = serviciosTemp.getPrimero();
				DoubleLinkedList<String> stringTemp = strings.giveInfo().getListaServices();
				for (int y = 0; y < serviciosTemp.Size(); y++) {
					{

						for (int w = 0; w < stringTemp.Size(); w++) {
							Node<String> servId = serviceIDs2.getPrimero();
							for (int r = 0; r < serviceIDs2.getSize(); r++) {
								try {
									if (servId.giveInfo().equals(stringTemp.get(w))) {
										respuesta.agregarComienzo(serviciosTemp.get(y).giveInfo());
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							servId = servId.darNext();
						}
					}

					strings = strings.darNext();
				}
			}
			agen = agen.darNext();
		}

		return respuesta;
	}

	// 2B
	public ILista<VOTrips> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) throws Exception
	{
		DoubleLinkedList<String> serviceIDs2 = new DoubleLinkedList<>();
		DoubleLinkedList<VOTrips> listaFinal = new DoubleLinkedList<>();
		VORoute sujeto = null;
		boolean bool = false;

		for (int i = 0; i <= getCalendarDates().Size(); i++) {
			if (getCalendarDates().get(i).giveInfo().getDate().equals(fecha)
					&& getCalendarDates().get(i).giveInfo().getException_type() == 1) {
				serviceIDs2.agregarComienzo(getCalendarDates().get(i).giveInfo().getService_id() + "");
			}


		}
		
		Node<VORoute> rutax = getRoutes().getPrimero();
		for (int i = 0; i < getRoutes().Size(); i++) 
		{
			if((rutax.giveInfo().getRouteId()+"").equals(idRuta))
			{
				sujeto = rutax.giveInfo();
			}

		}
		
		Node<VOTrips> tripx = sujeto.getListaTrips().getPrimero();
		for (int i = 0; i < sujeto.getListaTrips().Size() ; i++) 
		{
			if(delayAfter(tripx.giveInfo())==2)
			{
				listaFinal.agregarComienzo(tripx.giveInfo());
			}
		}
		
		int n = listaFinal.Size();
		for (int j = 1; j < n; j++) {
			int key;
			key = Integer.parseInt(listaFinal.get(j).giveInfo().getTripID());
			int i = j - 1;
			while ((i > -1) && (Integer.parseInt(listaFinal.get(i).giveInfo().getTripID()) > key)) {
				listaFinal.swap(listaFinal.get(i + 1), listaFinal.get(i));
				i--;
			}
			VOTrips variable = listaFinal.get(i + 1).giveInfo();
			variable = listaFinal.get(j).giveInfo();
		}
		
		return listaFinal;
		
		

	}

	public int delayAfter(VOTrips trip)
	{
		DoubleLinkedList<Retardo> ret = trip.getListaRetardos();
		int sisenor = 0;
		Node<Retardo> recontranodo = ret.getPrimero();
		while(recontranodo != null)
		{
			if(recontranodo.giveInfo().getDelayTime()<0)
			{
				sisenor = 1;
				Node<Retardo> x = recontranodo;
				while(x.darNext() != null)
				{
					Retardo xx = x.giveInfo();
					if(x.darNext().giveInfo().getDelayTime()>0)
					{
						sisenor = 2;
						return sisenor;
					}
					x = x.darNext();
				}
				return sisenor;

			}

			recontranodo = recontranodo.darNext();

		}

		return sisenor;
	}



	// 3B

	public ILista<RangoHora> ITSretardoHoraRuta(String idRuta, String Fecha) {
		boolean bool = false;
		double[][] multiArray = new double[24][(getTrips().Size() / 24)];
		DoubleLinkedList<Integer> listafinal = new DoubleLinkedList<>();
		VORoute rutax = null;
		Node<VORoute> rutas = getRoutes().getPrimero();
		for (int x = 0; x < getRoutes().Size(); x++) {
			if (idRuta.equals(rutas.giveInfo().getRouteId() + "")) {
				rutax = rutas.giveInfo();
			}
			rutas = rutas.darNext();
		}
		retardo(rutax);

		int contador = 0;
		Node<VOTrips> tripsis = rutax.getListaTrips().getPrimero();
		for (int i = 0; i < rutax.getListaTrips().Size(); i++) {
			Node<Retardo> retx = tripsis.giveInfo().getListaRetardos().getPrimero();
			for (int j = 0; j < tripsis.giveInfo().getListaRetardos().Size(); j++) {
				try {
					getListaRangoHora().get(retx.giveInfo().getHoraDelay()).giveInfo().getListaRetardos()
					.agregarComienzo(retx.giveInfo());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				retx = retx.darNext();
			}
			tripsis = tripsis.darNext();
			contador++;
		}

		int contadorx = 0;

		Node<RangoHora> horax = getListaRangoHora().getPrimero();
		for (int i = 0; i < getListaRangoHora().Size(); i++) {
			if (contador < horax.giveInfo().sumaTotal()) {

			}
		}

		DoubleLinkedList<RangoHora> listaRx = new DoubleLinkedList<RangoHora>();

		Node<RangoHora> rxr = getListaRangoHora().getPrimero();
		for (int i = 0; i < getListaRangoHora().Size(); i++) {
			listaRx.agregarFinal(rxr.giveInfo());
		}

		// public static void insertionSort(int array[]) {
		try {
			int n = listaRx.Size();
			for (int j = 1; j < n; j++) {
				double key;
				key = listaRx.get(j).giveInfo().sumaTotal();
				int i = j - 1;
				while ((i > -1) && (listaRx.get(i).giveInfo().sumaTotal() > key)) {
					listaRx.swap(listaRx.get(i + 1), listaRx.get(i));
					i--;
				}
				RangoHora variable = listaRx.get(i + 1).giveInfo();
				variable = listaRx.get(j).giveInfo();
			}
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listaRx;

	}

	// 4B
	public ILista<VOTrips> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) throws Exception {

		DoubleLinkedList<VOTrips> respuesta = new DoubleLinkedList<VOTrips>();
		DoubleLinkedList<String> serviceIDs2 = new DoubleLinkedList<>();
		DoubleLinkedList<VORoute> rutasLista = new DoubleLinkedList<>();
		DoubleLinkedList<VORoute> rutasLista2 = new DoubleLinkedList<>();
		DoubleLinkedList<VORoute> rutasLista3 = new DoubleLinkedList<>();
		DoubleLinkedList<VOTrips> tripsLista = new DoubleLinkedList<>();

		for (int i = 0; i <= getCalendarDates().Size(); i++) {
			if (getCalendarDates().get(i).giveInfo().getDate().equals(fecha)
					&& getCalendarDates().get(i).giveInfo().getException_type() == 1) {
				serviceIDs2.agregarComienzo(getCalendarDates().get(i).giveInfo().getService_id() + "");
			}
		}

		Node<VORoute> ruta = getRoutes().getPrimero();
		for (int i = 0; i < getRoutes().Size(); i++) {

			Node<String> servicesRoutes = ruta.giveInfo().getListaServices().getPrimero();
			for (int j = 0; j < ruta.giveInfo().getListaServices().Size(); j++) {

				Node<String> service = serviceIDs2.getPrimero();
				for (int j2 = 0; j2 < serviceIDs2.Size(); j2++) {

					if (service.giveInfo().equals(servicesRoutes.giveInfo())) {
						rutasLista.agregarComienzo(ruta.giveInfo());
					}

				}
				service = service.darNext();
			}
			servicesRoutes = servicesRoutes.darNext();
		}
		ruta = ruta.darNext();

		boolean finalB = false;

		for (int i = 0; i < rutasLista.Size() && !finalB; i++) {
			boolean idOrigenB = false;
			boolean idDestinoB = false;
			Node<VORoute> rutas = rutasLista.getPrimero();
			for (int j = 0; j < rutas.giveInfo().getListaTrips().Size(); j++) {
				Node<VOTrips> trips = rutas.giveInfo().getListaTrips().getPrimero();
				for (int k = 0; k < trips.giveInfo().getListaStopTimes().Size(); k++) {
					Node<VOStoptimes> stopT = trips.giveInfo().getListaStopTimes().getPrimero();

					if (stopT.giveInfo().getStopID() == Integer.parseInt(idDestino)) {
						idDestinoB = true;
					}
					if (stopT.giveInfo().getStopID() == Integer.parseInt(idDestino)) {
						idOrigenB = true;
					}

				}
				if (idOrigenB == true && idDestinoB == true) {
					rutasLista2.agregarComienzo(rutas.giveInfo());
					finalB = true;
				} else {
					idOrigenB = false;
					idDestinoB = false;

				}

			}
		}
		Node<VORoute> rutas = rutasLista2.getPrimero();

		for (int i = 0; i < rutasLista2.Size(); i++) {
			boolean idOrigenB = false;
			boolean idDestinoB = false;
			Node<VOTrips> trips = rutas.giveInfo().getListaTrips().getPrimero();
			for (int j = 0; j < rutas.giveInfo().getListaTrips().Size(); j++) {
				Node<VOStoptimes> stopT = trips.giveInfo().getListaStopTimes().getPrimero();
				int sequenceOrigin = 0;
				int sequenceDestiny = 0;
				String fechaOrigin = "";
				String fechaDestino = "";
				for (int j2 = 0; j2 < trips.giveInfo().getListaStopTimes().Size(); j2++) {

					if (Integer.parseInt(stopT.giveInfo().getSequence()) == Integer.parseInt(idDestino)) {
						sequenceDestiny = Integer.parseInt(stopT.giveInfo().getSequence());
						fechaDestino = stopT.giveInfo().getArrivalTime();
						idDestinoB = true;
					}
					if (Integer.parseInt(stopT.giveInfo().getSequence()) == Integer.parseInt(idOrigen)) {
						sequenceOrigin = Integer.parseInt(stopT.giveInfo().getSequence());
						fechaOrigin = stopT.giveInfo().getDepartureTime();
						idOrigenB = true;
					}

				}
				if (idOrigenB == true && idDestinoB == true) {
					if ((sequenceOrigin < sequenceDestiny)
							&& (fechaOrigin.compareTo(horaInicio) > 0 && fechaDestino.compareTo(horaFin) < 0)) {
						tripsLista.agregarComienzo(trips.giveInfo());
					}
				} else {
					idOrigenB = false;
					idDestinoB = false;

				}

			}
		}

		Node<VOTrips> tripx = tripsLista.get(0);
		DoubleLinkedList<VOTrips> listaFinal = new DoubleLinkedList<VOTrips>();
		for (int i = 0; i < tripsLista.Size(); i++) {
			if (tripRangoHora(tripx.giveInfo(), horaInicio, horaFin) == true) {
				listaFinal.add(tripx);
			}

		}

		return listaFinal;
	}

	// 5B

	public VORoute ITSrutaMenorRetardo(String fecha) {
		shapesATrips();
		DoubleLinkedList<String> serviceIDs2 = new DoubleLinkedList<>();
		DoubleLinkedList<VORoute> listaRoutes = new DoubleLinkedList<>();
		boolean bool = false;

		for (int i = 0; i <= getCalendarDates().Size(); i++) {
			try {
				if (getCalendarDates().get(i).giveInfo().getDate().equals(fecha)
						&& getCalendarDates().get(i).giveInfo().getException_type() == 1) {
					serviceIDs2.agregarComienzo(getCalendarDates().get(i).giveInfo().getService_id() + "");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		Node<VORoute> rutax = getRoutes().getPrimero();
		for (int i = 0; i < getRoutes().Size(); i++) 
		{ 
			Node<String> servicex = rutax.giveInfo().getListaServices().getPrimero();
			for (int j = 0; j < rutax.giveInfo().getListaServices().Size() && !bool; j++) 
			{
				Node<String> servx2 = serviceIDs2.getPrimero();
				for (int j2 = 0; j2 < serviceIDs2.Size(); j2++) 
				{
					if(servx2.equals(servicex.giveInfo()))
					{
						listaRoutes.agregarComienzo(rutax.giveInfo());
						bool = true;
					}
					servx2 = servx2.darNext();
				}
				servicex = servicex.darNext();
			}
			rutax = rutax.darNext();
		}

		Double contador = Double.MAX_VALUE;
		Node<VORoute> rx = listaRoutes.getPrimero();
		VORoute respu = null;
		for (int i = 0; i < listaRoutes.Size(); i++) 
		{
			if(contador<RetrasosPromedioRuta(rx.giveInfo()))
			{
				respu = rx.giveInfo();
				contador = RetrasosPromedioRuta(rx.giveInfo());

			}
		}

		return respu;


	}

	public double RetrasosPromedioRuta(VORoute ruta) {
		double respu = 0;
		double respu2 = 0;
		int contador = 0;
		Node<VOTrips> tripx = ruta.getListaTrips().getPrimero();
		for (int i = 0; i < ruta.getListaTrips().Size(); i++) {

			Node<VOShapes> shape1 = tripx.giveInfo().getListaShapes().getPrimero();
			Node<VOShapes> shape2 = tripx.giveInfo().getListaShapes().getUltimo();

			tripx.giveInfo().setMaxDistanciaStops(
					getDistance(shape1.giveInfo().getShape_pt_lat(), shape1.giveInfo().getShape_pt_lon(),
							shape2.giveInfo().getShape_pt_lat(), shape2.giveInfo().getShape_pt_lon()));

			tripx = tripx.darNext();

			respu = totalRetardosTrip(tripx.giveInfo()) / tripx.giveInfo().getMaxDistanciaStops();
			contador++;
		}
		respu2 = respu/contador;

		return respu2;

	}

	public double totalRetardosTrip(VOTrips trip) {
		double respu = 0;

		Node<Retardo> retx = trip.getListaRetardos().getPrimero();
		for (int i = 0; i < trip.getListaRetardos().Size(); i++) {
			respu += retx.giveInfo().getDelayTime();
		}

		return respu;
	}

	public void shapesATrips() {
		Node<VOShapes> shapex = getShapes().getPrimero();
		for (int i = 0; i < getShapes().Size(); i++) {
			Node<VOTrips> tripx = getTrips().getPrimero();
			for (int j = 0; j < getTrips().Size(); j++) {
				if (tripx.giveInfo().getShapeID().equals(shapex.giveInfo().getShape_id())) {
					tripx.giveInfo().getListaShapes().agregarComienzo(shapex.giveInfo());
				}

				tripx = tripx.darNext();
			}

			shapex = shapex.darNext();
		}

	}

	public boolean tripRangoHora(VOTrips trip, String inicio, String fin) throws Exception {
		int iniciox = cambiarFechas(inicio);
		int finx = cambiarFechas(fin);
		boolean respu = false;
		DoubleLinkedList<BusService> listaBus = new DoubleLinkedList<BusService>();

		Node<BusService> bus = getListaBusService().get(0);
		for (int i = 0; i < getListaBusService().Size(); i++) {
			if ((bus.giveInfo().getTripID()).equals(trip.getTripID())) {
				listaBus.add(bus);
			}
			bus = bus.darNext();
		}

		Node<BusService> busx = listaBus.get(0);
		for (int i = 0; i < listaBus.Size(); i++) {

			String hora = busx.giveInfo().getRecordedTime();
			if ((cambiarFechas(hora) > (iniciox)) && (cambiarFechas(hora) < finx)) {
				respu = true;
			}

			busx = busx.darNext();
		}

		return respu;
	}

	public int cambiarFechas(String hora) {
		String[] horax = hora.split(":");
		int hora24 = Integer.parseInt(horax[0]);
		if (hora.endsWith("pm")) {
			hora24 = hora24 + 12;
		}

		return hora24;
	}

	public void busUpdatesALista(VOTrips x) {

		Node<BusService> bs = getListaBusService().getPrimero();
		for (int i = 0; i < getListaBusService().Size(); i++) {
			if (bs.giveInfo().getTripID().equals(x.getTripID() + "")) {
				x.getListaBusServices().agregarComienzo(bs.giveInfo());
			}
		}
	}

	public void retardo(VORoute ruta) {

		Node<VOTrips> tripx = ruta.getListaTrips().getPrimero();
		for (int i = 0; i < ruta.getListaTrips().Size(); i++) {

			Node<VOStop> stopx = ruta.getListaStops().getPrimero();
			for (int j2 = 0; j2 < ruta.getListaStops().Size(); j2++) {
				double cortica1 = Double.MAX_VALUE;
				double cortica2 = Double.MAX_VALUE;
				double avg = 0;
				int arrivalTime = 0;
				BusService bx1 = null;
				BusService bx2 = null;

				Node<BusService> busx = tripx.giveInfo().getListaBusServices().getPrimero();
				for (int j3 = 0; j3 < tripx.giveInfo().getListaBusServices().Size(); j3++) {
					double distanciaEntreStopYUpdate = getDistance(Double.parseDouble(stopx.giveInfo().getStopLat()),
							Double.parseDouble(stopx.giveInfo().getStopLon()), busx.giveInfo().getLatitude(),
							busx.giveInfo().getLongitud());
					if (distanciaEntreStopYUpdate < cortica1) {
						cortica1 = distanciaEntreStopYUpdate;
						bx1 = busx.giveInfo();
					} else if (distanciaEntreStopYUpdate < cortica2) {
						cortica2 = distanciaEntreStopYUpdate;
						bx2 = busx.giveInfo();
					}
					avg = (HorasAMilisegundos(bx1) + HorasAMilisegundos(bx2)) / 2;

					Node<VOStoptimes> stopTx = tripx.giveInfo().getListaStopTimes().getPrimero();
					for (int x = 0; x < tripx.giveInfo().getListaStopTimes().Size(); x++) {
						if (stopTx.giveInfo().getTripID().equals(tripx.giveInfo().getTripID())) {
							arrivalTime = HorasAMilisegundos2(stopTx.giveInfo().getArrivalTime());
						}
					}
					if (arrivalTime < avg) {
						tripx.giveInfo().getListaRetardos().agregarComienzo(new Retardo(
								stopx.giveInfo().getStopId() + "", (arrivalTime - avg), HorasBusService(bx1)));
					}

					busx = busx.darNext();
				}

				tripx = tripx.darNext();
			}
			stopx = stopx.darNext();

		}

	}

	public int HorasAMilisegundos2(String service) {
		int respu = 0;
		String[] serx = service.split(" ");
		String[] sery = serx[0].split(":");
		int h = Integer.parseInt(sery[0]);
		int m = Integer.parseInt(sery[0]);
		int s = Integer.parseInt(sery[0]);

		respu = (h * 36000000) + (m * 60000) + (s * 1000);

		return respu;

	}

	public int HorasAMilisegundos(BusService service) {
		int respu = 0;
		String[] serx = service.getRecordedTime().split(" ");
		String[] sery = serx[0].split(":");
		int h = Integer.parseInt(sery[0]);
		int m = Integer.parseInt(sery[0]);
		int s = Integer.parseInt(sery[0]);

		respu = (h * 36000000) + (m * 60000) + (s * 1000);

		return respu;

	}

	public int HorasBusService(BusService service) {
		int respu = 0;
		String[] serx = service.getRecordedTime().split(" ");
		String[] sery = serx[0].split(":");
		int h = Integer.parseInt(sery[0]);
		int m = Integer.parseInt(sery[0]);
		int s = Integer.parseInt(sery[0]);

		if (serx[1].contains("pm")) {
			h = h + 12;
		}

		return h;

	}

}
