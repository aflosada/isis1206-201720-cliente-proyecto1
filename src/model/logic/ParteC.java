package model.logic;

import model.data_structures.*;
import model.vo.*;


public class ParteC extends STSManager{


	public DoubleLinkedList<VOStop> punto4(String fechaDada)
	{
		DoubleLinkedList<VOStop> listaGigantesca = new DoubleLinkedList<VOStop>();
		DoubleLinkedList<VOStop> listaNoTanGigantesca = new DoubleLinkedList<VOStop>();
		DoubleLinkedList<VOStop> listaConsiderablementeMenosGigantesca = new DoubleLinkedList<VOStop>();



		Node<VORoute> rutas = getRoutes().getPrimero();
		for (int i = 0; i < getRoutes().Size(); i++) 
		{
			DoubleLinkedList<VOStop> stops = rutas.giveInfo().getListaStops();
			Node<VOStop> stop = stops.getPrimero();
			for (int j = 0; j < stops.Size(); j++) {

				listaGigantesca.agregarComienzo(stop.giveInfo());
				stop = stop.darNext();
			}
			rutas = rutas.darNext();
		}

		Node<VOStop> stopi = listaGigantesca.getPrimero();
		for (int i = 0; i < listaGigantesca.Size(); i++) {

			Node<VOStop> stopj = listaGigantesca.getPrimero();
			boolean encontrado = false;
			for (int j = 1; j < listaGigantesca.Size() && !encontrado; j++) 
			{
				if(stopi.giveInfo().getStopId() == stopj.giveInfo().getStopId())
				{
					listaNoTanGigantesca.agregarComienzo(stopj.giveInfo());
					encontrado = false;
				}
				stopj = stopj.darNext();
			}

			stopi = stopi.darNext();
		}

		Node<VOCalendarDates> calD =  getCalendarDates().getPrimero();
		//		for (int i = 0; i < array.length; i++) 

		return listaConsiderablementeMenosGigantesca;


	}






	//3C 
	public ILista<Retardo> ITSretardosViaje(String fecha, String idViaje) 
	{
		DoubleLinkedList<Retardo> respu = new DoubleLinkedList<>();

		DoubleLinkedList<Retardo> rets = new DoubleLinkedList<>();
		VOTrips sujeto = null;
		boolean bool = false;
		Node<VOTrips> tripx = getTrips().getPrimero();
		for (int i = 0; i < getTrips().Size() && !bool ; i++) 
		{
			if(tripx.giveInfo().getTripID().equals(idViaje))
			{
				sujeto = tripx.giveInfo();
				bool = true;
			}
		}

		for (int i = 0; i < tripx.giveInfo().getListaRetardos().Size(); i++) 
		{
			Node<Retardo> rx = tripx.giveInfo().getListaRetardos().getPrimero();
			rets.agregarComienzo(rx.giveInfo());
		}


		for (int i = 0; i <= getCalendarDates().Size(); i++) {
			try {
				if (getCalendarDates().get(i).giveInfo().getDate().equals(fecha)&& getCalendarDates().get(i).giveInfo().getException_type() == 0) 
					return new DoubleLinkedList<>();

				else{
					respu = rets;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rets;


	}


	/*
	 *4C 
	 */
	public DoubleLinkedList<VOStop> ITSParadasCompartidas(String fechaDada)
	{
		DoubleLinkedList<String> service = new DoubleLinkedList<String>();
		Node<VOCalendarDates> calD = getCalendarDates().getPrimero();
		DoubleLinkedList<VORoute> routes1 = new DoubleLinkedList<VORoute>();
		DoubleLinkedList<DoubleLinkedList<VOStop>> listaStopsUnicos = new DoubleLinkedList<DoubleLinkedList<VOStop>>();


		for (int i = 0; i < getCalendarDates().Size(); i++) {
			if(calD.giveInfo().getDate().equals(fechaDada))
			{
				service.agregarComienzo(calD.giveInfo().getService_id()+"");
			}
			calD = calD.darNext();
		}


		Node<VORoute> ruta = getRoutes().getPrimero();
		for (int j = 0; j < getRoutes().Size(); j++) 
		{
			Node<String> svce = service.getPrimero();
			for (int i = 0; i < service.Size(); i++) 
			{
				Node<String> servic = ruta.giveInfo().getListaServices().getPrimero();
				for (int j2 = 0; j2 < ruta.giveInfo().getListaServices().Size(); j2++) 
				{
					if(servic.giveInfo().equals(svce.giveInfo()))
					{
						routes1.agregarComienzo(ruta.giveInfo());
					}
					servic = servic.darNext();
				}
				svce = svce.darNext();

			}
			ruta = ruta.darNext();
		}

		for (int i = 0; i < routes1.Size(); i++) {
			DoubleLinkedList<VOStop> miniStop = new DoubleLinkedList<VOStop>();


		}




		return null;
	}
}