package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import com.google.gson.*;
import model.data_structures.*;
import model.vo.*;



public class STSManager implements API.ISTSManager {

	private DoubleLinkedList<VOAgency> Agency = new DoubleLinkedList<>();
	private DoubleLinkedList<VOStop> Stops;
	private DoubleLinkedList<VOTrips> Trips;
	private DoubleLinkedList<VOCalendar> Calendario;
	private DoubleLinkedList<VOCalendarDates> CalendarDates;
	private DoubleLinkedList<VORoute> Routes = new DoubleLinkedList<>();;
	private DoubleLinkedList<VOShapes> Shapes;
	private DoubleLinkedList<VOFeedInfo> FeedInfo;
	private DoubleLinkedList<VOStoptimes> StopTimes;
	private DoubleLinkedList<VOTransfers> Transfers;
	private Queue colaBusServicios;
	private JsonParser json;
	private DoubleLinkedList<BusService> listaBusService = new DoubleLinkedList<>();
	private DoubleLinkedList<StopEstimService> listaStopEst;
	private DoubleLinkedList<RangoHora> listaRangoHora;

	// private DoubleLinkedList<T> stopsEstimServicesLista;
	// private DoubleLinkedList<T> agencyLista;
	// private DoubleLinkedList<T> calendarLista;
	// private DoubleLinkedList<T> feedInfo;
	// private DoubleLinkedList<T> routesLista;
	// private DoubleLinkedList<T> shapesLista;
	// private DoubleLinkedList<T> stopTimesLista;
	// private DoubleLinkedList<T> stopsLista;
	// private DoubleLinkedList<T> transfersLista;
	// private DoubleLinkedList<T> tripsLista;

	@Override
	public void ITScargarGTFS() {
		File file = new File("data_busservice");
		File[] files = file.listFiles();

		File file2 = new File("data_stopService");
		File[] files2 = file2.listFiles();

		// TODO Auto-generated method stub
		cargarAgency("data/agency.txt");
		cargarCalendar("data/calendar.txt");
		cargarCalendarDates("data/calendar_dates.txt");
		cargarFeedInfo("data/feed_info.txt");
		cargarRoutes("data/routes.txt");
		cargarShapes("data/shapes.txt");
		cargarStops("data/stops.txt");
		cargarStopTimes("data/stop_times.txt");
		cargarTransfers("data/transfers.txt");
		cargarTrips("data/trips.txt");
		for (int i = 0; i < files.length; i++) 
		{
			cargarBusService(files[i]);
		}
		for (int i = 0; i < files2.length; i++) 
		{
			cargarStopEstimService(files2[i]);
		}
	}

	@Override
	public void ITScargarTR(String fecha) 
	{
		// TODO Auto-generated method stub

	}
	
	public void cargarListaRangoHora()
	{
		int contador = 0;
		while(contador<24)
		{
			listaRangoHora.agregarFinal(new RangoHora(contador+"", contador+1+""));
			contador++;
		}
	}

	public DoubleLinkedList<RangoHora> getListaRangoHora() {
		return listaRangoHora;
	}

	public void setListaRangoHora(DoubleLinkedList<RangoHora> listaRangoHora) {
		this.listaRangoHora = listaRangoHora;
	}

	@Override
	public ILista<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) 
	{
		DoubleLinkedList<VORoute> listaTemp = new DoubleLinkedList<VORoute>();
		DoubleLinkedList<VORoute> listaFinal = new DoubleLinkedList<VORoute>();
		// TODO Auto-generated method stub
		Iterator<VOAgency> agencyIt = Agency.iterator();
		while(agencyIt.hasNext())
		{
			VOAgency actual = agencyIt.next();
			if(actual.getAgency_name().equals(nombreEmpresa))
			{
				listaTemp = actual.getListaRutas();
				System.out.println(listaTemp.Size());
			}
		}
		System.out.println(listaTemp.Size());
		DoubleLinkedList<VOTrips> listaTrips = new DoubleLinkedList<VOTrips>();
		DoubleLinkedList<VOCalendar> listaCalendariosFinal = new DoubleLinkedList<VOCalendar>();
		
		 DoubleLinkedList<Integer> listaCalendarsPre = darListaFechas(fecha);
		Iterator<Integer> calendarIt = listaCalendarsPre.iterator();
		while(calendarIt.hasNext())
		{
			Integer actual = calendarIt.next();
			System.out.println(actual);
			Iterator<VOTrips> tripsIt = Trips.iterator();
			while(tripsIt.hasNext())
			{
				System.out.println(tripsIt.hasNext());
				VOTrips actual2 = tripsIt.next();
				System.out.println(actual2);
				System.out.println(actual);
				System.out.println(actual2.getServiceID());

				if((actual) == Integer.parseInt(actual2.getServiceID()))
				{
					listaTrips.agregarComienzo(actual2);
				}
			}
		}
		
		
		
		Iterator<VORoute> routeIt = listaTemp.iterator();
		while(routeIt.hasNext())
		{
			VORoute rutaActual = routeIt.next();
			DoubleLinkedList<VOTrips> tripsRuta = rutaActual.getListaTrips();
			Iterator<VOTrips> tripsIt = tripsRuta.iterator();
			while(tripsIt.hasNext())
			{
				Iterator<VOTrips> trips2It = listaTrips.iterator();
					while(trips2It.hasNext())
					{
						VOTrips actual3 = trips2It.next();
						VOTrips tripActual = tripsIt.next();
						if(tripActual.getTripID() == actual3.getTripID())
						{
							listaFinal.agregarComienzo(rutaActual);
						}
				}
			}
		}
		return listaFinal;
	}

	@Override
	public ILista<VOTrips> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		
				DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
				
				DoubleLinkedList<Date> timesBus = new DoubleLinkedList<Date>();
				
				DoubleLinkedList<Date> timesStops = new DoubleLinkedList<Date>();
				
				int rutaDada = Integer.parseInt(idRuta);
				
				VORoute vorouteDada = null;
				
				Iterator<VORoute> routeIt = Routes.iterator();
				while(routeIt.hasNext())
				{
					VORoute actual = routeIt.next();
					if(actual.getRouteId() == rutaDada)
					{
						vorouteDada = actual;
					}
				}
				
				DoubleLinkedList<VOTrips> tripsRutaDada = vorouteDada.getListaTrips();
				
				Iterator<BusService> busIt = listaBusService.iterator();
				
				Queue<BusService> qBusesEnRuta = new Queue<BusService>();
				
				while(busIt.hasNext())
				{
					BusService actual = busIt.next();
					if(Integer.parseInt(actual.getRouteNo()) == rutaDada )
					{
						qBusesEnRuta.enqueue(actual);
						try 
						{
							timesBus.agregarFinal(sdf.parse(actual.getRecordedTime()));
						} catch (Exception e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				Iterator<VOTrips> itTrips = tripsRutaDada.iterator();
				
				DoubleLinkedList<BusService> listaBuses = new DoubleLinkedList<BusService>();
				DoubleLinkedList<VOStoptimes> listStopTimes = new DoubleLinkedList<VOStoptimes>();
				
				while(itTrips.hasNext())
				{
					VOTrips actual = itTrips.next();
					listaBuses.agregarFinal(actual.getListaBusServices().getPrimero().giveInfo());
					actual.getListaBusServices().getPrimero().darNext().cambiarPrev(listaBuses.getPrimero());;
					
					listStopTimes.agregarFinal(actual.getListaStopTimes().getPrimero().giveInfo());
					actual.getListaStopTimes().getPrimero().darNext().cambiarPrev(listStopTimes.getPrimero());
				}
				
				Iterator<BusService> itlistb = listaBuses.iterator();
				Iterator<VOStoptimes> itlists = listStopTimes.iterator();
				
				SimpleDateFormat sfd = new SimpleDateFormat("HH:mm:ss");
				
				DoubleLinkedList<Integer> asdasd= new DoubleLinkedList<Integer>();
				DoubleLinkedList<Integer> listaConNumeroDeEstrelladas = new DoubleLinkedList<Integer>();
				try 
				{
					while (itlists.hasNext()) 
					{
						VOStoptimes actual2 = itlists.next();
						Date actualDateStop = sfd.parse(actual2.getArrivalTime());				
						int contador = 0;
						while (itlistb.hasNext()) 
						{
							BusService actual = itlistb.next();

							Date actualDateBus = sfd.parse(actual.getRecordedTime());
							
							if (actualDateBus.compareTo(actualDateStop) > 0) 
							{
								asdasd.agregarFinal(Integer.parseInt(actual.getTripID()));
								
								contador++;
							}
						}
						listaConNumeroDeEstrelladas.agregarFinal(contador);
					}
				}
				catch (Exception e) 
				{
					System.out.println("Error Date");
				}
				
				Iterator<VOTrips> itTorips = tripsRutaDada.iterator();
				Iterator<Integer> itASSD = asdasd.iterator();
				Iterator<Integer> itEstrellitas2 = listaConNumeroDeEstrelladas.iterator();
				
				DoubleLinkedList<VOTrips> listaFinal = new DoubleLinkedList<VOTrips>();
				DoubleLinkedList<Integer> listaConNumeroDeEstrelladas2 = new DoubleLinkedList<Integer>();
				int[] arrEstrellitas = new int[Trips.getSize()];
				
				int contador = 0;
				
				while(itTorips.hasNext())
				{
					VOTrips actual = itTorips.next();
					while(itASSD.hasNext())
					{
						Integer actual2 = itASSD.next();
						Integer actual3= itEstrellitas2.next();
						contador++;
						if(Integer.parseInt(actual.getTripID()) == actual2)
						{
							listaFinal.agregarFinal(actual);
							arrEstrellitas[contador] = actual3;
						}
					}
				}
				
				int tamano = arrEstrellitas.length;
				
				for (int i = 0; i < tamano; i++) 
				{
					int max = 0;
					for (int j = 0; j < tamano; j++) 
					{
						if(max < arrEstrellitas[j])
						{
							int temp = arrEstrellitas[i];
							arrEstrellitas[i] = arrEstrellitas[j];
							arrEstrellitas[j] = temp;
						}
					}
					tamano--;
				}
				
				DoubleLinkedList<VOTrips> listaFinal2 = new DoubleLinkedList<VOTrips>();
				
				
				for (int i = 0; i < arrEstrellitas.length; i++) 
				{
					try 
					{
						listaFinal2.agregarFinal((listaFinal.get(i)).giveInfo());
					} 
					catch (Exception e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}		
				return listaFinal;
	}

	@Override
	public ILista<VOStop> ITSparadasRetrasadasFecha(String fecha) 
	{
		DoubleLinkedList<BusService> listaBuses = new DoubleLinkedList<BusService>();
		DoubleLinkedList<VOStoptimes> listStopTimes = new DoubleLinkedList<VOStoptimes>();
		
		DoubleLinkedList<Integer> fechasQueSirven = darListaFechas(fecha);
		
		Iterator<BusService> itlistb = listaBuses.iterator();
		Iterator<VOStoptimes> itlists = listStopTimes.iterator();
		
		SimpleDateFormat sfd = new SimpleDateFormat("HH:mm:ss");
		
		DoubleLinkedList<Integer> asdasd= new DoubleLinkedList<Integer>();
		
		try 
		{
			while (itlistb.hasNext()) 
			{
				BusService actual = itlistb.next();

				Date actualDateBus = sfd.parse(actual.getRecordedTime());
				while (itlists.hasNext()) 
				{
					VOStoptimes actual2 = itlists.next();
					Date actualDateStop = sfd.parse(actual2.getArrivalTime());

					if (actualDateBus.compareTo(actualDateStop) > 0) 
					{
						asdasd.agregarFinal(actual2.getStopID());
					}
				}
			}
		}
		catch (Exception e) 
		{
			System.out.println("Error Date");
		}
		
		DoubleLinkedList<VOStop> listaStops = new DoubleLinkedList<VOStop>();
		
		
		Iterator<VOStop> itStops = Stops.iterator();
		while(itStops.hasNext())
		{
			VOStop actual = itStops.next();
			Iterator<Integer> itasdasd = asdasd.iterator();
			while(itasdasd.hasNext())
			{
				Integer actual2 = itasdasd.next();
				if(actual.getStopId() == actual2)
				{
					listaStops.agregarFinal(actual);
				}
			}
		}
		
		
		sortStops(listaStops, listaStops.getPrimero(), listaStops.getUltimo());
		
		return listaStops;
	}

	@Override
	public ILista<VOTransfers> ITStransbordosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		DoubleLinkedList<Integer> listaFechas = darListaFechas(fecha);
		DoubleLinkedList<VOTrips> listaTrips = new DoubleLinkedList<VOTrips>();
		
		Iterator<Integer> it1 = listaFechas.iterator();
		Iterator<VOTrips> it2 = Trips.iterator();
		
		while(it2.hasNext())
		{
			VOTrips actual = it2.next();
			while(it1.hasNext())
				{
				Integer actual2 = it1.next();
				if(Integer.parseInt(actual.getServiceID()) == actual2)
				{
					listaTrips.agregarFinal(actual);
				}
			}
		}
		
		
		
		
		
		return null;
	}

	@Override
	public ILista<ILista<ILista<VOStop>>> ITSrutasPlanUtilizacion(String fecha, String horaInicio, String horaFin) 
	{
		// TODO Auto-generated method stub
		DoubleLinkedList<Integer> listaFechas = darListaFechas(fecha);
		
		String anio = horaInicio.substring(0, 1);
		String mes = horaInicio.substring(2, 3);
		String dia = horaInicio.substring(4, 5);
		
		String fechaFinal = dia+":"+mes+":"+anio;
		try
		{
		Date date = new SimpleDateFormat("HH/mm/ss").parse(horaInicio);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		String anio1 = horaFin.substring(0, 1);
		String mes2 = horaFin.substring(2, 3);
		String dia3 = horaFin.substring(4, 5);
		
		String fechaFinal4 = dia+":"+mes+":"+anio;
		try
		{
		Date date2 = new SimpleDateFormat("HH:mm:ss").parse(horaFin);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		Iterator<VOTrips> itTrips = Trips.iterator();
		Iterator<Integer> itFechas = listaFechas.iterator();
		
		Queue<VOTrips> queue = new Queue<VOTrips>();
		
		Queue<Integer> queueR = new Queue<Integer>();
		
		while(itTrips.hasNext())
		{
			VOTrips actual = itTrips.next();
			while(itFechas.hasNext())
			{
				Integer actual2 = itFechas.next();
				if(Integer.parseInt(actual.getServiceID()) == actual2)
				{
					queue.enqueue(actual);
					queueR.enqueue(actual.getRouteID());
				}
			}
		}
		
		Iterator<VORoute> rutaIt = Routes.iterator();
		
		Queue<VORoute> queuerutas = new Queue<VORoute>();
		
		while(rutaIt.hasNext())
		{
			VORoute actual = rutaIt.next();
			while(queueR.giveSize() != 0)
			{
				Integer actual2 = queueR.dequeue();
				if(actual.getRouteId() == actual2)
				{
					queuerutas.enqueue(actual);
				}
			}
		}
		
		Queue<VOTrips> queueT2 = new Queue<VOTrips>();
		DoubleLinkedList<DoubleLinkedList<VOStoptimes>> listaStops = new DoubleLinkedList<DoubleLinkedList<VOStoptimes>>();
		
		DoubleLinkedList<Integer> listaStopIDS = new DoubleLinkedList<Integer>();
		
		Iterator<VOStoptimes> itStops = StopTimes.iterator();
		while(queue.giveSize() != 0)
		{
			VOTrips actual = queue.dequeue();
			listaStops.agregarFinal(actual.getListaStopTimes());
			queueT2.enqueue(actual);
		}
		 Iterator<DoubleLinkedList<VOStoptimes>> listapailait = listaStops.iterator();
		while(queueT2.giveSize()!= 0)
		{
			VOTrips actual = queueT2.dequeue();
			while(listapailait.hasNext())
			{
				DoubleLinkedList<VOStoptimes> actual3 = listapailait.next();
				Iterator<VOStoptimes> sisa = actual3.iterator();
				while(sisa.hasNext())
				{
					VOStoptimes actual54 = sisa.next();
					listaStopIDS.agregarFinal(actual54.getStopID());
				}
			}
		}
		
		DoubleLinkedList<DoubleLinkedList<DoubleLinkedList<VOStop>>> listaFinal = new DoubleLinkedList<DoubleLinkedList<DoubleLinkedList<VOStop>>>();
		
		DoubleLinkedList<VOStop> listaFinalS = new DoubleLinkedList<VOStop>();
		Iterator<Integer> itstopIDS = listaStopIDS.iterator();
		Iterator<VOStop> itStops3 = Stops.iterator();
		while(itstopIDS.hasNext())
		{
			Integer actual = itstopIDS.next();
			while(itStops3.hasNext())
			{
				VOStop actual2 = itStops3.next();
				if(actual == actual2.getStopId())
				{
					listaFinalS.agregarFinal(actual2);
				}
			}
		}
		
		DoubleLinkedList<DoubleLinkedList<VOStop>> listaMenosPaila = new DoubleLinkedList<DoubleLinkedList<VOStop>>();
		while(queueR.giveSize() != 0)
		{
			Integer actual1 = queueR.dequeue();
			Iterator<Integer> itstopIDSx = listaStopIDS.iterator();
			while(itstopIDSx.hasNext())
			{
				Integer actual3 = itstopIDSx.next();
				Iterator<VOStop> itstopIDSxd = listaFinalS.iterator();
				while(itstopIDSxd.hasNext())
				{
					VOStop actual4 = itstopIDSxd.next();
					listaMenosPaila.agregarFinal(listaFinalS);
					listaFinal.agregarFinal(listaMenosPaila);
				}
			}
		}
		return listaFinal;
	}


	@Override
	public ILista<VOTrips> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<RangoHora> ITSretardoHoraRuta(String idRuta, String Fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTrips> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VORoute ITSrutaMenorRetardo(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void ITSInit() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOService> ITSserviciosMayorDistancia(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Retardo> ITSretardosViaje(String fecha, String idViaje) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOStop> ITSparadasCompartidas(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cargarAgency(String agencyFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {

			// FileReader fr = new FileReader(routesFile);
			// new BufferedReader(fr);
			br = new BufferedReader(new FileReader(agencyFile));
			Agency = new DoubleLinkedList<VOAgency>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				String agencyId = lineas[0];
				String agencyName = lineas[1];
				String agencyUrl = lineas[2];
				String agencyTZ = lineas[3];
				String agencyLang = lineas[4];
				VOAgency vor = new VOAgency(agencyId, agencyName, agencyUrl, agencyTZ, agencyLang);
				Agency.agregarFinal(vor);
			}

			br.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// cargarRoutes("data/routes.txt");
	}

	@Override
	public void cargarRoutes(String routesFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {

			// FileReader fr = new FileReader(routesFile);
			// new BufferedReader(fr);
			br = new BufferedReader(new FileReader(routesFile));
			Routes = new DoubleLinkedList<VORoute>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				int routId = Integer.parseInt((lineas[0]));
				String agencyId = lineas[1];
				String routeShortName = lineas[2];
				String routeLongName = lineas[3];
				String routeDescription = lineas[4];
				String routeType = lineas[5];
				String routeUrl = lineas[6];
				String routeColor = lineas[7];
				String routeTextColor = lineas[8];

				VORoute vor = new VORoute(routId, agencyId, routeShortName, routeLongName, routeDescription, routeType,
						routeUrl, routeColor, routeTextColor);
				Routes.agregarFinal(vor);

				/*
				 * Se agrega la rutas a la lista en agencias que se encarga de
				 * teer las respectivas rutas de la agencia.
				 */
				Node<VOAgency> node = Agency.getPrimero();
				VOAgency x = node.giveInfo();

				while (node.darNext() != null) {
					if (x.getAgency_id().equals(agencyId)) {
						node.giveInfo().getListaRutas().agregarFinal(vor);
					}
					node = node.darNext();
				}
			}

			br.close();

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		cargarTrips("data/trips.txt");
	}

	@Override
	public void cargarTrips(String tripsFile) {

		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {
			br = new BufferedReader(new FileReader(tripsFile));
			Trips = new DoubleLinkedList<VOTrips>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				int routId = Integer.parseInt((lineas[0]));
				String serviceID = lineas[1];
				String tripID = lineas[2];
				String headsign = lineas[3];
				String sName = lineas[4];
				int direccionID = Integer.parseInt(lineas[5]);
				String blockID = lineas[6];
				String shapeID = lineas[7];
				String wheelchairAcc = lineas[8];
				String bicycleAcc = lineas[9];

				VOTrips vor = new VOTrips(routId, serviceID, tripID, headsign, sName, direccionID, blockID, shapeID, wheelchairAcc, bicycleAcc);
				Trips.agregarComienzo(vor);

				/*
				 * Se agrega la rutas a la lista en agencias que se encarga de
				 * teer las respectivas rutas de la agencia.
				 */
				Node<VORoute> node = Routes.getPrimero();
				VORoute x = node.giveInfo();
				boolean respu = false;
				while (node != null && !respu) {
					if (routId == x.id()) {
						node.giveInfo().getListaTrips().agregarComienzo(vor);
						node.giveInfo().getListaServices().agregarComienzo(serviceID);

					}
					node = node.darNext();

				}
			}

			br.close();

		} catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void cargarStops(String stopsFile) {
		// TODO Auto-generated method stub
		String linea;
		String splitBy = ",";
		VOStop vor = null;

		// TODO Auto-generated method stub
		try {

			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			Stops = new DoubleLinkedList<VOStop>();
			br.readLine();
			int x = 0;
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				int stopID = Integer.parseInt((lineas[0]));
				String stopCode = lineas[1];
				String stopName = lineas[2];
				String stopDesc = lineas[3];
				String stopLat = lineas[4];
				String stopLon = lineas[5];
				String zoneID = lineas[6];
				String stopURL = lineas[7];
				String locationType = lineas[8];

				if (lineas.length == 10) {
					String parentStation = lineas[9];
					vor = new VOStop(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL,
							locationType, parentStation);
					Stops.agregarComienzo(vor);

				} else {
					vor = new VOStop(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL,
							locationType);
					Stops.agregarComienzo(vor);
				}
				



			}

			System.out.println(Stops.Size());
			br.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

	}

	public DoubleLinkedList<VOTrips> unicosViajes(DoubleLinkedList<VOTrips> viajesLista)
	{
		DoubleLinkedList<VOTrips> viajesUnicos = new DoubleLinkedList<>();
		Node<VOTrips> trip = viajesLista.getPrimero();
		
		for (int i = 0; i < viajesLista.Size(); i++) {
			if(viajesUnicos.Size() > 0)
			{
				boolean encontrado = false;
				Node<VOTrips> trip2 = viajesUnicos.getPrimero();
				for (int j = 0; j < viajesUnicos.Size() && !encontrado; j++) {
					if(trip.giveInfo().getShapeID() == trip2.giveInfo().getShapeID())
					{
						encontrado = true;
					}
					trip2 = trip2.darNext();
				}
				if(encontrado == false)
				{
					viajesUnicos.add(trip2);
				}
			}
			trip = trip.darNext();
		}
		
		return viajesUnicos;
	}
	
	
	public void agregarStopsARuta(VORoute ruta)
	{
		DoubleLinkedList<VOTrips> trips = unicosViajes(ruta.getListaTrips());
		
		Node<VOTrips> nodoTrips = trips.getPrimero();
		for (int i = 0; i < trips.Size(); i++) 
		{
			DoubleLinkedList<VOStoptimes> stopTLista = nodoTrips.giveInfo().getListaStopTimes();
			Node<VOStoptimes> stopT = stopTLista.getPrimero();
			for (int j = 0; j < stopTLista.Size(); j++) 
			{
				Node<VOStop> stopN = Stops.getPrimero();
				for (int j2 = 0; j2 < Stops.Size(); j2++) 
				{
					if(stopT.giveInfo().getStopID() == stopN.giveInfo().getStopId())
					{
						ruta.getListaStops().agregarComienzo(stopN.giveInfo());
					}
					
					stopN = stopN.darNext();
				}
				
				stopT = stopT.darNext();
			}
			nodoTrips = nodoTrips.darNext();
		}
		
				
	}

	@Override
	public void cargarCalendar(String calendarFile) {
		// TODO Auto-generated method stub

		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{
			br = new BufferedReader(new FileReader(calendarFile));
			Calendario = new DoubleLinkedList<VOCalendar>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				String serviceID = lineas[0];
				int monday = Integer.parseInt(lineas[1]);
				int tuesday = Integer.parseInt(lineas[2]);
				int wednesday = Integer.parseInt(lineas[3]);
				int thursday = Integer.parseInt(lineas[4]);
				int friday = Integer.parseInt(lineas[5]);
				int saturday = Integer.parseInt(lineas[6]);
				int sunday = Integer.parseInt(lineas[7]);
				String startDate = lineas[8];
				String pendDate = lineas[9];

				VOCalendar vor = new VOCalendar<>(serviceID, monday, tuesday, wednesday, thursday, friday, saturday,
						sunday, startDate, pendDate);
				Calendario.agregarFinal(vor);
			}

			br.close();

		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public void cargarCalendarDates(String calendarDatesFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {
			br = new BufferedReader(new FileReader(calendarDatesFile));
			CalendarDates = new DoubleLinkedList<VOCalendarDates>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				String serviceID = lineas[0];
				String date = lineas[1];
				int exceptionType = Integer.parseInt((lineas[2]));

				VOCalendarDates vor = new VOCalendarDates(serviceID, date, exceptionType);
				CalendarDates.agregarFinal(vor);
			}

			br.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	// @Override
	public void cargarShapes(String ShapesFile) {

		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		try {

			// FileReader fr = new FileReader(routesFile);
			// new BufferedReader(fr);
			br = new BufferedReader(new FileReader(ShapesFile));
			Shapes = new DoubleLinkedList<VOShapes>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				String shapeId = lineas[0];
				double shapeLat = Double.parseDouble(lineas[1]);
				double shapeLong = Double.parseDouble(lineas[2]);
				String sequence = lineas[3];
				double distance = Double.parseDouble(lineas[4]);

				VOShapes vor = new VOShapes(shapeId, shapeLat, shapeLong, sequence, distance);
				Shapes.agregarFinal(vor);

			}

			br.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void cargarFeedInfo(String feedInfoFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {

			// FileReader fr = new FileReader(routesFile);
			// new BufferedReader(fr);
			br = new BufferedReader(new FileReader(feedInfoFile));
			FeedInfo = new DoubleLinkedList<VOFeedInfo>();
			br.readLine();
			while ((linea = br.readLine()) != null) {
				String[] lineas = linea.split(splitBy);
				String feedPublisherName = lineas[0];
				String feedPublisherURL = lineas[1];
				String feedLang = lineas[2];
				String startDate = lineas[3];
				String endDate = lineas[4];
				String feedVersion = lineas[5];

				VOFeedInfo vor = new VOFeedInfo(feedPublisherName, feedPublisherURL, feedLang, startDate, endDate,
						feedVersion);
				FeedInfo.agregarFinal(vor);

			}

			br.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void cargarStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{

			br = new BufferedReader(new FileReader(stopTimesFile));
			StopTimes = new DoubleLinkedList<VOStoptimes>();
			br.readLine();
			int contador = 0;
			while ((linea = br.readLine()) != null) 
			{
				String[] lineas = linea.split(splitBy);
				String tripID = lineas[0];
				String departureTime = lineas[1];
				String arrivalTime = lineas[2];
				int stopID = Integer.parseInt(lineas[3]);
				String sequence = lineas[4];
				String headsign = lineas[5];
				String pickUpType = lineas[6];
				String dropOffType = lineas[7];

				VOStoptimes vor = new VOStoptimes(tripID, departureTime, arrivalTime, stopID, sequence, headsign,
						pickUpType, dropOffType);
				StopTimes.agregarFinal(vor);

				Iterator<VOTrips> tripsIt = Trips.iterator();
				System.out.println(contador);
				while (tripsIt.hasNext()) 
				{
					VOTrips tripActual = tripsIt.next();
					if (tripID.equals(tripActual.getTripID()))
					{
						tripActual.getListaStopTimes().agregarComienzo(vor);
						
					}
					System.out.println(tripActual.getListaStopTimes().getSize());
				}
				linea = br.readLine();
				contador++;
			}
			br.close();

		} catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void cargarTransfers(String transfersFile) {
		// TODO Auto-generated method stub
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try {

			br = new BufferedReader(new FileReader(transfersFile));
			Transfers = new DoubleLinkedList<VOTransfers>();
			br.readLine();
			while ((linea = br.readLine()) != null) 
			{
				String[] lineas = linea.split(splitBy);
				String fromStopID = lineas[0];
				String toStopID = lineas[1];
				int transferType = Integer.parseInt(lineas[2]);
				

				if (lineas.length <= 3)
				{
					VOTransfers vor = new VOTransfers(fromStopID, toStopID, transferType, null);
					Transfers.agregarFinal(vor);
				} 
				else 
				{
					String transferTime = lineas[3];
					VOTransfers vor = new VOTransfers(fromStopID, toStopID, transferType);
					Transfers.agregarFinal(vor);
				}

				
			}
			br.close();
		} 
		catch (IOException e1)
{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void cargarBusService(File rtFile) {

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			BusService[] bus = gson.fromJson(reader, BusService[].class);
			for (int i = 0; i < bus.length; i++) {
				listaBusService.agregarComienzo((bus[i]));
				bus[i].cambiarFechas();
				System.out.println(bus[i].getRecordedTime()+"");
				System.out.println(bus[i].getRouteNo());
			}
			

		} catch (FileNotFoundException e) {

		} catch (Exception e) {

		} 
		finally 
		{
			try 
			{
				reader.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void cargarStopEstimService(File rtFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			StopEstimService[] bus = gson.fromJson(reader, StopEstimService[].class);

			VOStop stopsazo = null;
			String nombre = rtFile+"";
			String[] nombre2 = nombre.split("_");
			String[] nombre3 = nombre2[3].split("_");
			String nombreFinal = nombre3[0];
			Node<VOStop> stop = Stops.getPrimero();
			for (int j = 0; j < Stops.Size(); j++) {
				
				if(stop.giveInfo().getStopCode().equals(nombreFinal))
				{
					stopsazo = stop.giveInfo();
				}
				stop = stop.darNext();
			}
			
			for (int i = 0; i < bus.length; i++) {
				
				stopsazo.getListaStopsEst().agregarComienzo(bus[i]);
				listaStopEst.agregarComienzo((bus[i]));
				
			}
			
			
			
			

		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Error");
		} catch (Exception e) {

		} 
		finally 
		{
			try 
			{
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			}
		}

	}

	public DoubleLinkedList<VOAgency> getAgency() {
		return Agency;
	}

	public void setAgency(DoubleLinkedList<VOAgency> agency) {
		Agency = agency;
	}

	public DoubleLinkedList<VOStop> getStops() {
		return Stops;
	}

	public void setStops(DoubleLinkedList<VOStop> stops) {
		Stops = stops;
	}

	public DoubleLinkedList<VOTrips> getTrips() {
		return Trips;
	}

	public void setTrips(DoubleLinkedList<VOTrips> trips) {
		Trips = trips;
	}

	public DoubleLinkedList<VOCalendar> getCalendar() {
		return Calendario;
	}

	public void setCalendar(DoubleLinkedList<VOCalendar> calendar) {
		Calendario = calendar;
	}

	public DoubleLinkedList<VOCalendarDates> getCalendarDates() {
		return CalendarDates;
	}

	public void setCalendarDates(DoubleLinkedList<VOCalendarDates> calendarDates) {
		CalendarDates = calendarDates;
	}

	public DoubleLinkedList<VORoute> getRoutes() {
		return Routes;
	}

	public void setRoutes(DoubleLinkedList<VORoute> routes) {
		Routes = routes;
	}

	public DoubleLinkedList<VOShapes> getShapes() 
	{
		return Shapes;
	}

	public void setShapes(DoubleLinkedList<VOShapes> shapes) {
		Shapes = shapes;
	}

	public DoubleLinkedList<VOFeedInfo> getFeedInfo() {
		return FeedInfo;
	}

	public void setFeedInfo(DoubleLinkedList<VOFeedInfo> feedInfo) {
		FeedInfo = feedInfo;
	}

	public DoubleLinkedList<VOStoptimes> getStopTimes() {
		return StopTimes;
	}

	public void setStopTimes(DoubleLinkedList<VOStoptimes> stopTimes) {
		StopTimes = stopTimes;
	}

	public DoubleLinkedList<VOTransfers> getTransfers() {
		return Transfers;
	}

	public void setTransfers(DoubleLinkedList<VOTransfers> transfers) {
		Transfers = transfers;
	}

	public Queue getColaBusServicios() {
		return colaBusServicios;
	}

	public void setColaBusServicios(Queue colaBusServicios) {
		this.colaBusServicios = colaBusServicios;
	}

	public DoubleLinkedList<BusService> getListaBusService() {
		return listaBusService;
	}

	public void setListaBusService(DoubleLinkedList<BusService> listaBusService) {
		this.listaBusService = listaBusService;
	}

		
	public DoubleLinkedList<Integer> darListaFechas(String fecha)
	{
		String anio = fecha.substring(0, 3);
		String mes = fecha.substring(4, 5);
		String dia = fecha.substring(6, 7);
		
		String fechaFinal = dia+"/"+mes+"/"+anio;
		Date date = null;
		try
		{
		date = new SimpleDateFormat("dd/M/YYYY").parse(fechaFinal);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		Calendar calendario =  Calendar.getInstance();
		calendario.setTime(date);
		
		int diaDeLaSemana = calendario.get(Calendar.DAY_OF_WEEK);
		DoubleLinkedList<Integer> listaFechas = new DoubleLinkedList<Integer>();
		Iterator<VOCalendar> calendarIt = Calendario.iterator();
		
		System.out.println(diaDeLaSemana);
		
			
			if(diaDeLaSemana == 2)
			{
				int serviceId = 1;
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
			}
			
			else if(diaDeLaSemana == 7)
			{
				int serviceId2 = 2;
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId2)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
			}
			else if(diaDeLaSemana == 1)
			{
				int serviceId2 = 3;
				VOCalendar actual = calendarIt.next();
				System.out.println(actual);
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId2)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
				System.out.println(listaFechas.Size());
			}
			else
			{
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
					actual = calendarIt.next();
				}
				
				System.out.println(listaFechas.Size());
			}
		
			System.out.println(listaFechas.Size());
		 return listaFechas;
	}
	
	public String cambiarTiempoDeBus(String s)
	{
		String rta = "";
		String[] arreglo = s.split(" ");
		if(s.contains("pm"))
		{
			arreglo[0] = "" +Integer.parseInt(arreglo[0]) + 12;
		}
		for(int i = 0; i< arreglo.length; i++)
		{
			rta += arreglo[i];
		}
		
		return rta;
	}
	
	public double darPromedioPosicion(double x, double y)
	{
		double rta = 0;
		rta = Math.pow(((x*x)+(y*y)),0.5);
		return rta;
	}
	
	public void sortStops(DoubleLinkedList<VOStop> a, Node<VOStop> lo, Node<VOStop> hi)
	 {
		 if (hi.darNext() == lo) 
		 {
			 return;
		 }
		 Node<VOStop> j = partitionStops(a, lo, hi);
		 sortStops(a, lo, j.darPrev());
		 sortStops(a, j.darNext(), hi);
	 }
	
	public Node<VOStop> partitionStops(DoubleLinkedList<VOStop> a, Node<VOStop> lo, Node<VOStop> hi)
	 { 
		 Node<VOStop> i = lo,  j = hi; 
		 Node<VOStop> v = a.getPrimero(); 
	  while (true)
	  { 
		  while (less(i.darNext(), v)) 
		  {
			  if (i == hi) 
			  {
				  break;
			  }
		  }
		  while (less(v, j.darPrev())) 
		  {
			  if (j == lo) 
			  {
				  break;
			  }
		  }
		  if (i.darNext() == j) 
		  {
			  break;
		  }
	  		a.exch(a, i, j);
	  	}
	  		a.exch(a, lo, j); 
	  		return j; 
	 }
	 

	private boolean less(Node<VOStop> i, Node<VOStop> v) 
	{
		boolean rta = false;
		if(i.giveInfo().getStopId() <= v.giveInfo().getStopId())
		{
			rta = true;
		}
		return rta;
	}

	@Override
	public VORoute ITSrutaMenorRetardo(String idRuta) {
		// TODO Auto-generated method stub
		return null;
	}




	
}
