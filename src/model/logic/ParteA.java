package model.logic;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Calendar;

import com.google.gson.JsonParser;

import model.data_structures.DoubleLinkedList;
import model.data_structures.ILista;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.logic.STSManager;
import model.vo.BusService;
import model.vo.StopEstimService;
import model.vo.VOAgency;
import model.vo.VOCalendar;
import model.vo.VOCalendarDates;
import model.vo.VOFeedInfo;
import model.vo.VORoute;
import model.vo.VOSchedule;
import model.vo.VOShapes;
import model.vo.VOStop;
import model.vo.VOStoptimes;
import model.vo.VOTransfers;
import model.vo.VOTrips;

public class ParteA extends STSManager

{
	
	private DoubleLinkedList<VOAgency> Agency;
	private RingList<VOStop> Stops;
	private RingList<VOTrips> Trips;
	private DoubleLinkedList<VOCalendar> VOCalendar;
	private DoubleLinkedList<VOCalendarDates> CalendarDates;
	private DoubleLinkedList<VORoute> Routes;
	private DoubleLinkedList<VOShapes> Shapes;
	private DoubleLinkedList<VOFeedInfo> FeedInfo;
	private DoubleLinkedList<VOStoptimes> StopTimes;
	private DoubleLinkedList<VOTransfers> Transfers;
	private Queue colaBusServicios;
	private JsonParser json;
	private DoubleLinkedList<BusService> listaBusService;
	private DoubleLinkedList<VOSchedule> scheduleLista;
	
	
	/*
	 * 1A
	 * @see model.logic.STSManager#ITSrutasPorEmpresa(java.lang.String, java.lang.String)
	 */
	
	public ILista<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) 
	{
		DoubleLinkedList<VORoute> listaTemp = new DoubleLinkedList<VORoute>();
		DoubleLinkedList<VORoute> listaFinal = new DoubleLinkedList<VORoute>();
		// TODO Auto-generated method stub
		Iterator<VOAgency> agencyIt = Agency.iterator();
		while(agencyIt.hasNext())
		{
			VOAgency actual = agencyIt.next();
			if(actual.getAgency_name().equals(nombreEmpresa))
			{
				listaTemp = actual.getListaRutas();
				System.out.println(listaTemp.Size());
			}
		}
		System.out.println(listaTemp.Size());
		DoubleLinkedList<VOTrips> listaTrips = new DoubleLinkedList<VOTrips>();
		DoubleLinkedList<VOCalendar> listaCalendariosFinal = new DoubleLinkedList<VOCalendar>();
		
		 DoubleLinkedList<Integer> listaCalendarsPre = darListaFechas(fecha);
		Iterator<Integer> calendarIt = listaCalendarsPre.iterator();
		while(calendarIt.hasNext())
		{
			Integer actual = calendarIt.next();
			System.out.println(actual);
			Iterator<VOTrips> tripsIt = Trips.iterator();
			while(tripsIt.hasNext())
			{
				System.out.println(tripsIt.hasNext());
				VOTrips actual2 = tripsIt.next();
				System.out.println(actual2);
				System.out.println(actual);
				System.out.println(actual2.getServiceID());

				if((actual) == Integer.parseInt(actual2.getServiceID()))
				{
					listaTrips.agregarComienzo(actual2);
				}
			}
		}
		
		
		
		Iterator<VORoute> routeIt = listaTemp.iterator();
		while(routeIt.hasNext())
		{
			VORoute rutaActual = routeIt.next();
			DoubleLinkedList<VOTrips> tripsRuta = rutaActual.getListaTrips();
			Iterator<VOTrips> tripsIt = tripsRuta.iterator();
			while(tripsIt.hasNext())
			{
				Iterator<VOTrips> trips2It = listaTrips.iterator();
					while(trips2It.hasNext())
					{
						VOTrips actual3 = trips2It.next();
						VOTrips tripActual = tripsIt.next();
						if(tripActual.getTripID() == actual3.getTripID())
						{
							listaFinal.agregarComienzo(rutaActual);
						}
				}
			}
		}
		return listaFinal;
	}
	
	
	public DoubleLinkedList<Integer> darServiceIdsQueSirven(String fecha)
	{
		String anio = fecha.substring(0, 3);
		String mes = fecha.substring(4, 5);
		String dia = fecha.substring(6, 7);
		
		String fechaFinal = dia+"/"+mes+"/"+anio;
		Date date = null;
		try
		{
		date = new SimpleDateFormat("dd/M/YYYY").parse(fechaFinal);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		Calendar calendario =  Calendar.getInstance();
		calendario.setTime(date);
		
		int diaDeLaSemana = calendario.get(Calendar.DAY_OF_WEEK);
		DoubleLinkedList<Integer> listaFechas = new DoubleLinkedList<Integer>();
		Iterator<VOCalendar> calendarIt = VOCalendar.iterator();
		
		System.out.println(diaDeLaSemana);
		
			
			if(diaDeLaSemana == 2)
			{
				int serviceId = 1;
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
			}
			
			else if(diaDeLaSemana == 7)
			{
				int serviceId2 = 2;
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId2)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
			}
			else if(diaDeLaSemana == 1)
			{
				int serviceId2 = 3;
				VOCalendar actual = calendarIt.next();
				System.out.println(actual);
				while(calendarIt.hasNext())
				{
					if((Integer.parseInt(actual.getService_id()) == serviceId2)) 
					{
						actual = calendarIt.next();
					}
					else
					{
						listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
						actual = calendarIt.next();
					}
				}
				System.out.println(listaFechas.Size());
			}
			else
			{
				VOCalendar actual = calendarIt.next();
				while(calendarIt.hasNext())
				{
					listaFechas.agregarComienzo(Integer.parseInt(actual.getService_id()));
					actual = calendarIt.next();
				}
				
				System.out.println(listaFechas.Size());
			}
		
			System.out.println(listaFechas.Size());
		 return listaFechas;
	}
	
	/*
	 * (non-Javadoc)
	 * 2A
	 * @see model.logic.STSManager#ITSviajesRetrasadosRuta(java.lang.String, java.lang.String)
	 */
	public ILista<VOTrips> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		
		DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		
		DoubleLinkedList<Date> timesBus = new DoubleLinkedList<Date>();
		
		DoubleLinkedList<Date> timesStops = new DoubleLinkedList<Date>();
		
		int rutaDada = Integer.parseInt(idRuta);
		
		VORoute vorouteDada = null;
		
		Iterator<VORoute> routeIt = Routes.iterator();
		while(routeIt.hasNext())
		{
			VORoute actual = routeIt.next();
			if(actual.getRouteId() == rutaDada)
			{
				vorouteDada = actual;
			}
		}
		
		DoubleLinkedList<VOTrips> tripsRutaDada = vorouteDada.getListaTrips();
		
		Iterator<BusService> busIt = listaBusService.iterator();
		
		Queue<BusService> qBusesEnRuta = new Queue<BusService>();
		
		while(busIt.hasNext())
		{
			BusService actual = busIt.next();
			if(Integer.parseInt(actual.getRouteNo()) == rutaDada )
			{
				qBusesEnRuta.enqueue(actual);
				try 
				{
					timesBus.agregarFinal(sdf.parse(actual.getRecordedTime()));
				} catch (ParseException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		Iterator<VOTrips> itTrips = tripsRutaDada.iterator();
		
		DoubleLinkedList<BusService> listaBuses = new DoubleLinkedList<BusService>();
		DoubleLinkedList<VOStoptimes> listStopTimes = new DoubleLinkedList<VOStoptimes>();
		
		while(itTrips.hasNext())
		{
			VOTrips actual = itTrips.next();
			listaBuses.agregarFinal(actual.getListaBusServices().getPrimero().giveInfo());
			actual.getListaBusServices().getPrimero().darNext().cambiarPrev(listaBuses.getPrimero());;
			
			listStopTimes.agregarFinal(actual.getListaStopTimes().getPrimero().giveInfo());
			actual.getListaStopTimes().getPrimero().darNext().cambiarPrev(listStopTimes.getPrimero());
		}
		
		Iterator<BusService> itlistb = listaBuses.iterator();
		Iterator<VOStoptimes> itlists = listStopTimes.iterator();
		
		SimpleDateFormat sfd = new SimpleDateFormat("HH:mm:ss");
		
		DoubleLinkedList<Integer> asdasd= new DoubleLinkedList<Integer>();
		DoubleLinkedList<Integer> listaConNumeroDeEstrelladas = new DoubleLinkedList<Integer>();
		try 
		{
			while (itlists.hasNext()) 
			{
				VOStoptimes actual2 = itlists.next();
				Date actualDateStop = sfd.parse(actual2.getArrivalTime());				
				int contador = 0;
				while (itlistb.hasNext()) 
				{
					BusService actual = itlistb.next();

					Date actualDateBus = sfd.parse(actual.getRecordedTime());
					
					if (actualDateBus.compareTo(actualDateStop) > 0) 
					{
						asdasd.agregarFinal(Integer.parseInt(actual.getTripID()));
						contador++;
					}
				}
				listaConNumeroDeEstrelladas.agregarFinal(contador);
			}
		}
		catch (Exception e) 
		{
			System.out.println("Error Date");
		}
		
		Iterator<VOTrips> itTorips = tripsRutaDada.iterator();
		Iterator<Integer> itASSD = asdasd.iterator();
		Iterator<Integer> itEstrellitas2 = listaConNumeroDeEstrelladas.iterator();
		
		DoubleLinkedList<VOTrips> listaFinal = new DoubleLinkedList<VOTrips>();
		DoubleLinkedList<Integer> listaConNumeroDeEstrelladas2 = new DoubleLinkedList<Integer>();
		int[] arrEstrellitas = new int[Trips.getSize()];
		
		int contador = 0;
		
		while(itTorips.hasNext())
		{
			VOTrips actual = itTorips.next();
			while(itASSD.hasNext())
			{
				Integer actual2 = itASSD.next();
				Integer actual3= itEstrellitas2.next();
				contador++;
				if(Integer.parseInt(actual.getTripID()) == actual2)
				{
					listaFinal.agregarFinal(actual);
					arrEstrellitas[contador] = actual3;
				}
			}
		}
		
		int tamano = arrEstrellitas.length;
		
		for (int i = 0; i < tamano; i++) 
		{
			int max = 0;
			for (int j = 0; j < tamano; j++) 
			{
				if(max < arrEstrellitas[j])
				{
					int temp = arrEstrellitas[i];
					arrEstrellitas[i] = arrEstrellitas[j];
					arrEstrellitas[j] = temp;
				}
			}
			tamano--;
		}
		
		DoubleLinkedList<VOTrips> listaFinal2 = new DoubleLinkedList<VOTrips>();
		
		
		for (int i = 0; i < arrEstrellitas.length; i++) 
		{
			try 
			{
				listaFinal2.agregarFinal((listaFinal.get(i)).giveInfo());
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		return listaFinal;
	}
	
	public String cambiarTiempoDeBus(String s)
	{
		String rta = "";
		String[] arreglo = s.split(":");
		if(s.contains("pm"))
		{
			arreglo[0] = "" +(Integer.parseInt(arreglo[0]) + 12);
		}
		for(int i = 0; i< arreglo.length; i++)
		{
			rta += arreglo[i];
		}
		
		return rta;
	}
	
	public double darPromedioPosicion(double x, double y)
	{
		double rta = 0;
		rta = Math.pow(((x*x)+(y*y)),0.5);
		return rta;
	}
	
	
	
	/*
	 * 3A
	 */
	public ILista<VOStop> ITSparadasRetrasadasFecha(String fecha) 
	{
		
		DoubleLinkedList<BusService> listaBuses = new DoubleLinkedList<BusService>();
		DoubleLinkedList<VOStoptimes> listStopTimes = new DoubleLinkedList<VOStoptimes>();
		
		DoubleLinkedList<Integer> fechasQueSirven = darListaFechas(fecha);
		
		Iterator<BusService> itlistb = listaBuses.iterator();
		Iterator<VOStoptimes> itlists = listStopTimes.iterator();
		
		SimpleDateFormat sfd = new SimpleDateFormat("HH:mm:ss");
		
		DoubleLinkedList<Integer> asdasd= new DoubleLinkedList<Integer>();
		
		try 
		{
			while (itlistb.hasNext()) 
			{
				BusService actual = itlistb.next();

				Date actualDateBus = sfd.parse(actual.getRecordedTime());
				while (itlists.hasNext()) 
				{
					VOStoptimes actual2 = itlists.next();
					Date actualDateStop = sfd.parse(actual2.getArrivalTime());

					if (actualDateBus.compareTo(actualDateStop) > 0) 
					{
						asdasd.agregarFinal(actual2.getStopID());
					}
				}
			}
		}
		catch (Exception e) 
		{
			System.out.println("Error Date");
		}
		
		DoubleLinkedList<VOStop> listaStops = new DoubleLinkedList<VOStop>();
		
		
		Iterator<VOStop> itStops = Stops.iterator();
		while(itStops.hasNext())
		{
			VOStop actual = itStops.next();
			Iterator<Integer> itasdasd = asdasd.iterator();
			while(itasdasd.hasNext())
			{
				Integer actual2 = itasdasd.next();
				if(actual.getStopId() == actual2)
				{
					listaStops.agregarFinal(actual);
				}
			}
		}
		
		
		sortStops(listaStops, listaStops.getPrimero(), listaStops.getUltimo());
		
		return listaStops;
	}
	
	public void sortStops(DoubleLinkedList<VOStop> a, Node<VOStop> lo, Node<VOStop> hi)
	 {
		 if (hi.darNext() == lo) 
		 {
			 return;
		 }
		 Node<VOStop> j = partitionStops(a, lo, hi);
		 sortStops(a, lo, j.darPrev());
		 sortStops(a, j.darNext(), hi);
	 }
	
	public Node<VOStop> partitionStops(DoubleLinkedList<VOStop> a, Node<VOStop> lo, Node<VOStop> hi)
	 { 
		 Node<VOStop> i = lo,  j = hi; 
		 Node<VOStop> v = a.getPrimero(); 
	  while (true)
	  { 
		  while (less(i.darNext(), v)) 
		  {
			  if (i == hi) 
			  {
				  break;
			  }
		  }
		  while (less(v, j.darPrev())) 
		  {
			  if (j == lo) 
			  {
				  break;
			  }
		  }
		  if (i.darNext() == j) 
		  {
			  break;
		  }
	  		a.exch(a, i, j);
	  	}
	  		a.exch(a, lo, j); 
	  		return j; 
	 }
	 

	private boolean less(Node<VOStop> i, Node<VOStop> v) 
	{
		boolean rta = false;
		if(i.giveInfo().getStopId() <= v.giveInfo().getStopId())
		{
			rta = true;
		}
		return rta;
	}
	
	public ILista<VOTransfers> ITStransbordosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}