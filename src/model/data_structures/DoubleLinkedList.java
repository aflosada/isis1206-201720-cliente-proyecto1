package model.data_structures;

import java.util.Iterator;

import model.vo.VORoute;

public class DoubleLinkedList<T> implements ILista {

	private Node<T> primero;
	private Node<T> ultimo;
	private int tamanho;

	public DoubleLinkedList() {
		primero = null;
		ultimo = null;
		tamanho = 0;
	}

	public int Size() {
		return tamanho;
	}

	public void agregarComienzo(T data) {
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if (primero == null) 
		{
			primero = nuevo;
			ultimo = primero;
		} else 
		{
			primero.cambiarPrev(nuevo);
			nuevo.cambiarNext(primero);
			primero = nuevo;
		}

	}

	public void agregarFinal(T data) {
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if (ultimo == null) {
			ultimo = nuevo;
			primero = ultimo;
		} else {
			ultimo.cambiarNext(nuevo);
			nuevo.cambiarPrev(ultimo);
			ultimo = nuevo;

		}

	}

	public Node<T> get(int index) throws Exception {
		if (index > Size() || index < 0) {
			throw new Exception("Not a valid index");
		} else {
			Node<T> buscador = primero;
			int contador = 0;
			while (contador < index) {
				buscador = buscador.darNext();
				contador++;
			}
			return buscador;
		}

	}

	public void eliminarNodo(int index) {

	}

	public boolean isEmpty() {
		if (primero == null || ultimo == null) {
			return true;
		}
		return false;
	}

	public boolean contieneA(Node<T> nodo) throws Exception {

		for (int i = 0; i < this.Size(); i++) {
			if (this.get(i).equals(nodo)) {
				return true;
			}
		}
		return false;
	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub

		Iterator<T> it = new Iterator<T>()

		{

			Node<T> firstNode = primero;
			Node<T> currentNode = null;

			int contador = 0;

			public boolean hasNext() {
				boolean respuesta = false;

				// TODO Auto-generated method stub
				if (isEmpty())
					respuesta = false;
				else if (currentNode == null) {
					respuesta = true;
				} else if (currentNode == ultimo) {
					respuesta = false;
				}
				return respuesta;
			}

			public T next() {
				T rta = null;
				// TODO Auto-generated method stub
				if (isEmpty()) {
					System.out.println("No hay m�s elementos");
				} else if (currentNode == null) {
					this.currentNode = firstNode;
					return currentNode.giveInfo();
				} else if (currentNode.darNext() == null) {
					System.out.println("No hay m�s elementos");
					;
				}
				return rta;
			}
		};
		return it;
	}

	public void add(Node n) {
		// TODO Auto-generated method stub

	}

	public void addAtEnd(Node n) {
		// TODO Auto-generated method stub

	}

	public void addAtK(Node n, int k) {
		// TODO Auto-generated method stub

	}

	public Node getElement(int k) {
		// TODO Auto-generated method stub
		return null;
	}

	public Node getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getSize() {
		// TODO Auto-generated method stub
		return tamanho;
	}

	public void delete() {
		// TODO Auto-generated method stub
		if (tamanho != 0) {

		}
		System.out.println("Numero de indice invalido");
	}

	public void deleteAtK(int index) {
		// TODO Auto-generated method stub
		try {
			Node<T> aEliminar = get(index);
			if (index == 0) {
				primero = aEliminar.darNext();
				System.out.println("Entra");
				tamanho--;
			} else if (index == Size() - 1) {
				ultimo = aEliminar.darPrev();
				aEliminar.darPrev().cambiarNext(null);
				tamanho--;
			} else {
				aEliminar.darNext().cambiarPrev(aEliminar.darPrev());
				aEliminar.darPrev().cambiarNext(aEliminar.darNext());
				tamanho--;
			}
		} catch (Exception e) {
			System.out.println("Numero de indice invalido");
		}
	}

	public Node<T> getPrimero() 
	{
		// TODO Auto-generated method stub
		return primero;
	}
	
	public Node<T> getUltimo()
	{
		return ultimo;
	}
	
	public void swap(Node<T> a, Node<T> b) {
	    if (a == b)
	        return;

	    if (a.darNext() == b) { 
	        a.cambiarNext(b.darNext());
	        b.cambiarPrev(a.darPrev());

	        if (a.darNext() != null)
	            a.darNext().cambiarPrev(a);

	        if (b.darPrev() != null)
	            b.darPrev().cambiarNext(b);


	        b.cambiarNext(a);
	        a.cambiarPrev(b);
	    } else {
	        Node<T> p = b.darPrev();
	        Node<T> n = b.darNext();

	        b.cambiarPrev(a.darPrev());
	        b.cambiarNext(a.darNext());

	        a.cambiarPrev(p);
	        a.cambiarNext(n);

	        if (b.darNext() != null)
	            b.darNext().cambiarPrev(b);
	        if (b.darPrev() != null)
	            b.darPrev().cambiarNext(b);

	        if (a.darNext() != null)
	            a.darNext().cambiarPrev(a);
	        if (a.darPrev() != null)
	            a.darPrev().cambiarNext(a);

	    }
	}
	public void cambiarPrimero(Node<T> a)
	{
		primero = a;
	}
	
	public void cambiarUltimo(Node<T> a)
	{
		ultimo = a;
	}
	
	public void exch(DoubleLinkedList<T> a, Node<T> i, Node<T> j) 
	{
		Node<T> temp = i;
		if(i == a.getPrimero() && j == a.getUltimo())
		{
			i.darNext().cambiarPrev(j);
			a.cambiarPrimero(j);
			a.cambiarUltimo(i);
			j.darPrev().cambiarNext(i);
		}
		else if(j == a.getUltimo() && i != a.getPrimero())
		{
			i.darNext().cambiarPrev(j);
			i.darPrev().cambiarNext(j);
			a.cambiarUltimo(i);
			j.darPrev().cambiarNext(i);
		}
		else if(j != a.getUltimo() && i == a.getPrimero())
		{
			i.darNext().cambiarPrev(j);
			a.cambiarPrimero(j);
			j.darPrev().cambiarNext(i);
			j.darNext().cambiarPrev(i);
		}
		else
		{
			i.darNext().cambiarPrev(j);
			i.darPrev().cambiarNext(j);
			j.darPrev().cambiarNext(i);
			j.darNext().cambiarPrev(i);
		}
		
	}

}
