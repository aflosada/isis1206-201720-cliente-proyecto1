package model.data_structures;

public class Stack<E> implements IStack<E>{

	Node<E> sale;
	
	private int size = 0;

	
	public Stack()
	{
		
		
	}
	
	public int giveSize()
	{
		return size;
	}
	
	public void push(E item) {
		Node<E> nodo = new Node<E>(item, null, null);
		if(giveSize() == 0)
		{
			sale = nodo;
			size++;
		}
		else
		{
			nodo.cambiarNext(sale);
			sale = nodo;
			size++;
		}
		
	}

	public E pop() {
		
		E respu = null;
		if(giveSize() == 1)
		{
			respu = sale.giveInfo();
			sale = null;
		}
		else
		{
			Node<E> temp = sale.darNext();
			respu = sale.giveInfo();
			sale.cambiarNext(null);
			sale = temp;
		}
		return respu;
	}

}
