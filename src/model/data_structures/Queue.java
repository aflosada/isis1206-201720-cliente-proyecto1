package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Queue<E> implements IQueue<E>{

	private Node<E> ultimoEnEntrar;
	private Node<E> primeroEnSalir;
	private int size = 0;

	
	public Node<E> getUltimoEnEntrar() {
		return ultimoEnEntrar;
	}

	public void setUltimoEnEntrar(Node<E> ultimoEnEntrar) {
		this.ultimoEnEntrar = ultimoEnEntrar;
	}

	public Node<E> getPrimeroEnSalir() {
		return primeroEnSalir;
	}

	public void setPrimeroEnSalir(Node<E> primeroEnSalir) {
		this.primeroEnSalir = primeroEnSalir;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
		
	public Queue ()
	{
		
	}
	
	public int giveSize()
	{
		return size;
	}
	
	

	public void enqueue(E item) 
	{
		Node<E> nodo = new Node<E>(item, null, null);
		if(giveSize() == 0)
		{
			ultimoEnEntrar = nodo;
			primeroEnSalir = nodo;
			size++;
		}
		else
		{
			nodo.cambiarNext(ultimoEnEntrar);
			ultimoEnEntrar = nodo;
			size++;
		}
		
	}


	public E dequeue() 
	{
		E respu = null;
		
		if(giveSize() == 1)
		{
			respu = ultimoEnEntrar.giveInfo();
			ultimoEnEntrar = null;
			primeroEnSalir = null;
			size--;
		}
		else
		{
			respu = primeroEnSalir.giveInfo();
			size--;
			asignarPrimeroEnSalir();
		}
		return respu;
	}
	
	public void asignarPrimeroEnSalir()
	{
		Node<E> nodox = ultimoEnEntrar;
		for(int i =0; i<giveSize(); i++)
		{
			nodox = nodox.darNext();
		}
		primeroEnSalir = nodox;
		primeroEnSalir.cambiarNext(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
	 
