package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILista<T> extends Iterable<T> 
{	
	public void add(Node <T> n);
	
	public void addAtEnd( Node <T> n);
	
	public void addAtK( Node <T> n, int k);
	
	public Node<T> getElement( int k);
	
	public Node<T> getCurrentElement();
	
	public int getSize();
	
	public void delete();
	
	public void deleteAtK(int k);

	public Iterator iterator();

}
