package model.data_structures;
import java.util.Iterator;

public class RingList <T> implements ILista<T>{

	private Node<T> primero;
	private int tamanho;

	public RingList()
	{
		primero = null;
		tamanho = 0;
	}

	public int Size()
	{
		return tamanho;
	}

	public void agregar(T data)
	{
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if(primero == null)
		{
			primero = nuevo;
		}
		else
		{
			primero.cambiarPrev(nuevo);
			nuevo.cambiarNext(primero);
			primero = nuevo;
		}

	}

	public Node<T> get(int index)
	{
		int contador1 = 0;
		int busq = index%Size();
		Node<T> buscador = primero;
		while(contador1<busq)
		{
			buscador = buscador.darNext();
			contador1 ++;
		}
		return buscador;
	}

	public void eliminarNodo(int index)
	{
		int indexX = index%Size();
		Node<T> aEliminar = get(index);
		
		if(indexX == 0)
		{
			primero = aEliminar.darNext();
			tamanho--;
		}

		else
		{
			aEliminar.darNext().cambiarPrev(aEliminar.darPrev());
			aEliminar.darPrev().cambiarNext(aEliminar.darNext());
			tamanho--;
		}
	}
	
	public boolean isEmpty()
	{
		if (primero == null)
		{
			return true;
		}
		return false;
	}
	
	public Node<T> getLast()
	{
		int last = this.tamanho-1;
		return this.get(last);
		
	}


	public Iterator<T> iterator() {
		// TODO Auto-generated method stub

		Iterator<T> it = new Iterator<T>()

		{

			Node<T> firstNode = primero;
			Node<T> currentNode = null;

			int contador = 0;

			public boolean hasNext() {
				boolean respuesta = false;

				// TODO Auto-generated method stub
				if (isEmpty())
					respuesta = false;
				else if (currentNode == null) {
					respuesta = true;
				} else if (currentNode == primero) {
					respuesta = false;
				}
				return respuesta;
			}

			public T next() {
				T rta = null;
				// TODO Auto-generated method stub
				if (isEmpty()) {
					System.out.println("No hay m�s elementos");
				} else if (currentNode == null) {
					this.currentNode = firstNode;
					return currentNode.giveInfo();
				} else if (currentNode.darNext() == null) {
					System.out.println("No hay m�s elementos");
					;
				}
				return rta;
			}
		};
		return it;
	}


	public void add(Node n) {
		// TODO Auto-generated method stub
		
	}

	public void addAtEnd(Node n) {
		// TODO Auto-generated method stub
		
	}

	public void addAtK(Node n, int k) {
		// TODO Auto-generated method stub
		
	}
	
	public Node getElement(int k) {
		// TODO Auto-generated method stub
		return null;
	}

	public Node getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void delete() {
		// TODO Auto-generated method stub
		
	}

	public void deleteAtK(int k) {
		// TODO Auto-generated method stub
		
	}

}
