package model.vo;

public class VOCalendarDates <T>
{
	int service_id; 
	String date;
	int exception_type;
	
	public VOCalendarDates(String pservice_id,String pdate,int pexception_type)
	{
		service_id = Integer.parseInt(pservice_id);
		date = pdate;
		exception_type = pexception_type;
	}

	public int getService_id() {
		return service_id;
	}

	public void setService_id(int service_id) {
		this.service_id = service_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getException_type() {
		return exception_type;
	}

	public void setException_type(int exception_type) {
		this.exception_type = exception_type;
	}
	
	
}
