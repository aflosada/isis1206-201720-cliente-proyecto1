package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int stopId;
	private String stopCode;
	private String stopName;
	private String stopDesc;
	private String stopLat;
	private String stopLon;
	private String zoneId;
	private String stopURL;
	private String locationType;
	private String parentStation;
	DoubleLinkedList<StopEstimService> listaStopsEst = new DoubleLinkedList<StopEstimService>();

	public VOStop(int id, String code, String name, String desc, String lat, String lon, String zoneID2, String URL, String locationT)
	{
	
		stopId = id;
		stopCode = code;
		stopName = name;
		stopDesc = desc;
		stopLat = lat;
		stopLon = lon;
		zoneId = zoneID2;
		stopURL = URL;
		locationType = locationT;
		
	}
	public VOStop(int stopID2, String stopCode2, String stopName2, String stopDesc2, String stopLat2, String stopLon2,
			String zoneID2, String stopURL2, String locationType2, String parentStation2) {
		stopId = stopID2;
		stopCode = stopCode2;
		stopName = stopName2;
		stopDesc = stopDesc2;
		stopLat = stopLat2;
		stopLon = stopLon2;
		zoneId = zoneID2;
		stopURL = stopURL2;
		locationType = locationType2;
		parentStation = parentStation2;
		// TODO Auto-generated constructor stub
	}
	

	public DoubleLinkedList<StopEstimService> getListaStopsEst() {
		return listaStopsEst;
	}
	public void setListaStopsEst(DoubleLinkedList<StopEstimService> listaStopsEst) {
		this.listaStopsEst = listaStopsEst;
	}
	
	public int getStopId() {
		return stopId;
	}
	public void setStopId(int stopId) {
		this.stopId = stopId;
	}
	public String getStopCode() {
		return stopCode;
	}
	public void setStopCode(String stopCode) {
		this.stopCode = stopCode;
	}
	public String getStopName() {
		return stopName;
	}
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}
	public String getStopDesc() {
		return stopDesc;
	}
	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}
	public String getStopLat() {
		return stopLat;
	}
	public void setStopLat(String stopLat) {
		this.stopLat = stopLat;
	}
	public String getStopLon() {
		return stopLon;
	}
	public void setStopLon(String stopLon) {
		this.stopLon = stopLon;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getStopURL() {
		return stopURL;
	}
	public void setStopURL(String stopURL) {
		this.stopURL = stopURL;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public String getParentStation() {
		return parentStation;
	}
	public void setParentStation(String parentStation) {
		this.parentStation = parentStation;
	}
	
	

}
