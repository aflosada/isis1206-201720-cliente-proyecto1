package model.vo;

import model.data_structures.DoubleLinkedList;

public class Retardo {
	
	private String stopID;
	private double delayTime;
	private int horaDelay;
	DoubleLinkedList<RangoHora> listaRango;
	
	public Retardo(String stopID, double delayTime, int horaDelay2) {
		super();
		this.stopID = stopID;
		this.delayTime = delayTime;
		horaDelay = horaDelay2;
		listaRango = new DoubleLinkedList<RangoHora>();
	}

	public String getStopID() {
		return stopID;
	}

	public void setStopID(String stopID) {
		this.stopID = stopID;
	}

	public double getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(double delayTime) {
		this.delayTime = delayTime;
	}

	public int getHoraDelay() {
		return horaDelay;
	}

	public void setHoraDelay(int horaDelay) {
		this.horaDelay = horaDelay;
	}

	public DoubleLinkedList<RangoHora> getListaRango() {
		return listaRango;
	}

	public void setListaRango(DoubleLinkedList<RangoHora> listaRango) {
		this.listaRango = listaRango;
	}
	

}
