package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOTrips {
	
	private int routeID;
	private String serviceID;
	private String tripID;
	private String headsign;
	private String Sname;
	private int direccionID;
	private String blockID;
	private String shapeID;
	private String wheelchairAcc;
	private String bicycleAcc;
	private double maxDistanciaStops;
	private DoubleLinkedList<VOStoptimes> listaStopTimes;
	private DoubleLinkedList<BusService> listaBusServices;
	private DoubleLinkedList<Retardo> listaRetardos;
	private DoubleLinkedList<VOShapes> listaShapes;
	

	public VOTrips(int rID, String sID, String tID, String Hsign, String sname, int dID, String bID,String shapeID2,String wAcc,String bAcc)
	{

		routeID = rID;
		serviceID = sID;
		tripID = tID;
		headsign = Hsign;
		Sname = sname;
		direccionID = dID;
		blockID =bID;
		shapeID = shapeID2;
		wheelchairAcc = wAcc;
		bicycleAcc = bAcc;
		listaStopTimes = new DoubleLinkedList<VOStoptimes>();
		listaBusServices = new DoubleLinkedList<BusService>();
		maxDistanciaStops = 0;
		
	}
	


	public DoubleLinkedList<BusService> getListaBusServices() {
		return listaBusServices;
	}

	public void setListaBusServices(DoubleLinkedList<BusService> listaBusService) {
		this.listaBusServices = listaBusService;
	}

	public String getShapeID() {
		return shapeID;
	}

	public void setShapeID(String shapeID) {
		this.shapeID = shapeID;
	}

	public DoubleLinkedList<VOStoptimes> getListaStopTimes() {
		return listaStopTimes;
	}

	public void setListaStopTimes(DoubleLinkedList<VOStoptimes> listaStopTimes) {
		this.listaStopTimes = listaStopTimes;
	}

	public int getRouteID() {
		return routeID;
	}

	public void setRouteID(int routeID) {
		this.routeID = routeID;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getTripID() {
		return tripID;
	}

	public void setTripID(String tripID) {
		this.tripID = tripID;
	}

	public String getHeadsign() {
		return headsign;
	}

	public void setHeadsign(String headsign) {
		this.headsign = headsign;
	}

	public String getSname() {
		return Sname;
	}

	public void setSname(String sname) {
		Sname = sname;
	}

	public int getDireccionID() {
		return direccionID;
	}

	public void setDireccionID(int direccionID) {
		this.direccionID = direccionID;
	}

	public String getBlockID() {
		return blockID;
	}

	public void setBlockID(String blockID) {
		this.blockID = blockID;
	}

	public String getWheelchairAcc() {
		return wheelchairAcc;
	}

	public void setWheelchairAcc(String wheelchairAcc) {
		this.wheelchairAcc = wheelchairAcc;
	}

	public String getBicycleAcc() {
		return bicycleAcc;
	}

	public void setBicycleAcc(String bicycleAcc) {
		this.bicycleAcc = bicycleAcc;
	}

	public DoubleLinkedList<Retardo> getListaRetardos() {
		return listaRetardos;
	}

	public void setListaRetardos(DoubleLinkedList<Retardo> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}



	public DoubleLinkedList<VOShapes> getListaShapes() {
		return listaShapes;
	}



	public void setListaShapes(DoubleLinkedList<VOShapes> listaShapes) {
		this.listaShapes = listaShapes;
	}



	public double getMaxDistanciaStops() {
		return maxDistanciaStops;
	}



	public void setMaxDistanciaStops(double maxDistanciaStops) {
		this.maxDistanciaStops = maxDistanciaStops;
	}
	
	
}
