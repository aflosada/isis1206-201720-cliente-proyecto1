package model.vo;

public class StopEstimService {

	private int routeNo;
	private String routeName;
	private String direction;
	private String routeMap;
	private String hRef;
	private VOSchedule[] schedule;
	
	
	
	
	public StopEstimService(int routeNo, String routeName, String direction, String routeMap, String hRef,
			VOSchedule[] schedule) {
		super();
		this.routeNo = routeNo;
		this.routeName = routeName;
		this.direction = direction;
		this.routeMap = routeMap;
		this.hRef = hRef;
		this.schedule = schedule;
	}
	
	
	
	public int getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(int routeNo) {
		this.routeNo = routeNo;
	}
	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getRouteMap() {
		return routeMap;
	}
	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}
	public String gethRef() {
		return hRef;
	}
	public void sethRef(String hRef) {
		this.hRef = hRef;
	}
	public VOSchedule[] getSchedule() {
		return schedule;
	}
	public void setSchedule(VOSchedule[] schedule) {
		this.schedule = schedule;
	}
	
	
}
