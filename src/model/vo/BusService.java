package model.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusService
{
	@SerializedName("VehicleNo")
	@Expose
	String vehicleNo;
	@SerializedName("TripID")
	@Expose
	String tripID;
	@SerializedName("RouteNo")
	@Expose
	String routeNo;
	@SerializedName("Direction")
	@Expose
	String direction;
	@SerializedName("Destination")
	@Expose
	String destination;
	@SerializedName("Pattern")
	@Expose
	String pattern;
	@SerializedName("RecordedTime")
	@Expose
	String recordedTime;
	@SerializedName("RouteMap")
	@Expose
	RouteMap routeMap;
	@SerializedName("Latitude")
	@Expose
	double latitude;
	@SerializedName("Longitud")
	@Expose
	double longitud;
	

	


	public String getVehicleNo() {
		return vehicleNo;
	}





	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}





	public String getTripID() {
		return tripID;
	}





	public void setTripID(String tripID) {
		this.tripID = tripID;
	}





	public String getRouteNo() {
		return routeNo;
	}





	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}





	public String getDirection() {
		return direction;
	}





	public void setDirection(String direction) {
		this.direction = direction;
	}





	public String getDestination() {
		return destination;
	}





	public void setDestination(String destination) {
		this.destination = destination;
	}





	public String getPattern() {
		return pattern;
	}





	public void setPattern(String pattern) {
		this.pattern = pattern;
	}





	public String getRecordedTime() {
		return recordedTime;
	}





	public void setRecordedTime(String recordedTime) {
		this.recordedTime = recordedTime;
	}





	public RouteMap getRouteMap() {
		return routeMap;
	}





	public void setRouteMap(RouteMap routeMap) {
		this.routeMap = routeMap;
	}





	public double getLatitude() {
		return latitude;
	}





	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}





	public double getLongitud() {
		return longitud;
	}





	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}





	public void cambiarFechas()
	{
		DateFormat readFormat = new SimpleDateFormat("hh:mm:ss aa");
		DateFormat writeFormat = new SimpleDateFormat("HH:mm:ss");
		
		Date date = null;
		try{
			date =readFormat.parse(recordedTime);
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		
		this.recordedTime = writeFormat.format(date);
	}

	

}
