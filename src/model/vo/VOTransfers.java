package model.vo;

public class VOTransfers 
{
	String from_stop_id;
	String to_stop_id;
	String min_transfer_time;
	int transfer_type;
	
	public VOTransfers(String from_stop_id2, String to_stop_id2, int transfer_type2, String min_transfer_time2) {

		from_stop_id = from_stop_id2;
		to_stop_id = to_stop_id2;
		min_transfer_time = min_transfer_time2;
		transfer_type = transfer_type2;
	}
	
	
	public VOTransfers(String fromStopID, String toStopID, int transferType) {
		// TODO Auto-generated constructor stub
		from_stop_id = fromStopID;
		to_stop_id = toStopID;
		transfer_type = transferType;
	}


	public String getFrom_stop_id() {
		return from_stop_id;
	}
	public void setFrom_stop_id(String from_stop_id) {
		this.from_stop_id = from_stop_id;
	}
	public String getTo_stop_id() {
		return to_stop_id;
	}
	public void setTo_stop_id(String to_stop_id) {
		this.to_stop_id = to_stop_id;
	}
	public String getMin_transfer_time() {
		return min_transfer_time;
	}
	public void setMin_transfer_time(String min_transfer_time) {
		this.min_transfer_time = min_transfer_time;
	}
	public int getTransfer_type() {
		return transfer_type;
	}
	public void setTransfer_type(int transfer_type) {
		this.transfer_type = transfer_type;
	}
	
	
}
