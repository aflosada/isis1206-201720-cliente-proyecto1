package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a route object
 */
public class VORoute 
{
	
	public void setRouteTextColor(String routeTextColor) {
		this.routeTextColor = routeTextColor;
	}

	private int routeId;
	private String agencyId;
	private String routeSName;
	private String routeLName;
	private String routeDesc;
	private String routeType;
	private String routeURL;
	private String routeColor;
	private String routeTextColor;
	private DoubleLinkedList<VOTrips> listaTrips;
	private DoubleLinkedList<String> listaServices;
	private DoubleLinkedList<VOStop> listaStops;
	
	

	public VORoute(int id, String ai , String sName, String lName, String desc, String type, String URL, String color, String textColor)
	{
		routeId = id;
		agencyId = ai;
		routeSName = sName;
		routeLName = lName;
		routeDesc = desc;
		routeType = type;
		routeURL = URL;
		routeColor = color;
		routeTextColor = textColor;
		listaTrips = new DoubleLinkedList<VOTrips>();
		listaServices = new DoubleLinkedList<String> ();
		listaStops = new DoubleLinkedList<VOStop>();
	}
	
	public int getRouteId() {
		return routeId;
	}



	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}



	public String getAgencyId() {
		return agencyId;
	}



	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}



	public String getRouteSName() {
		return routeSName;
	}



	public void setRouteSName(String routeSName) {
		this.routeSName = routeSName;
	}



	public String getRouteLName() {
		return routeLName;
	}



	public void setRouteLName(String routeLName) {
		this.routeLName = routeLName;
	}



	public String getRouteDesc() {
		return routeDesc;
	}



	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}



	public String getRouteType() {
		return routeType;
	}



	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}



	public String getRouteURL() {
		return routeURL;
	}



	public void setRouteURL(String routeURL) {
		this.routeURL = routeURL;
	}



	public String getRouteColor() {
		return routeColor;
	}



	public void setRouteColor(String routeColor) {
		this.routeColor = routeColor;
	}



	public String getRouteTextColor() {
		return routeTextColor;
	}

	
	
	
	public DoubleLinkedList<VOStop> getListaStops() {
		return listaStops;
	}



	public void setListaStops(DoubleLinkedList<VOStop> listaStops) {
		this.listaStops = listaStops;
	}



	public DoubleLinkedList<String> getListaServices() {
		return listaServices;
	}



	public void setListaServices(DoubleLinkedList<String> listaServices) {
		this.listaServices = listaServices;
	}



	public DoubleLinkedList<VOTrips> getListaTrips() {
		return listaTrips;
	}

	public void setListaTrips(DoubleLinkedList<VOTrips> listaTrips) {
		this.listaTrips = listaTrips;
	}

	/**
	 * @return id - Route's id number
	 */
	public int id() 
	{
		// TODO Auto-generated method stub
		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() 
	{
		// TODO Auto-generated method stub
		return routeSName;
	}
	

	
}
