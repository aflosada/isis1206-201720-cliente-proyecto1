package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;

public class RangoHora
{
	

	private String horaInicial;
	private String horaFinal;
	private DoubleLinkedList<Retardo> listaRetardos;

	public RangoHora(String horaInicial, String horaFinal) {
		super();
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
		
		listaRetardos = new DoubleLinkedList<Retardo>();
	}

	public String getHoraInicial() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getHoraFinal() {
		// TODO Auto-generated method stub
		return null;
	}

	public DoubleLinkedList<Retardo> getListaRetardos() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public double sumaTotal()
	{
		double respu = 0;
		Node<Retardo> retx = listaRetardos.getPrimero();
		for (int i = 0; i < listaRetardos.Size(); i++) 
		{
			respu += retx.giveInfo().getDelayTime();
		}
		
		return respu;
	}
}
