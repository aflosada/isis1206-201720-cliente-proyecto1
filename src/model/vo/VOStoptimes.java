package model.vo;

public class VOStoptimes {

	private String tripID;
	private String departureTime;
	private String arrivalTime;
	private int stopID;
	private String sequence;
	private String headsign;
	private String pickUpType;
	private String dropoffType;
	
	
	public VOStoptimes(String tID, String dTime, String aTime,int StopID, String sqnce, String hsign, String pType,String dpType )
	{
		
		tripID = tID;
		departureTime = dTime;
		arrivalTime = aTime;
		stopID = StopID;
		sequence = sqnce;
		headsign = hsign;
		pickUpType = pType;
		dropoffType = dpType;
		
	}


	public String getTripID() {
		return tripID;
	}


	public void setTripID(String tripID) {
		this.tripID = tripID;
	}


	public String getDepartureTime() {
		return departureTime;
	}


	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}


	public String getArrivalTime() {
		return arrivalTime;
	}


	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}


	public int getStopID() {
		return stopID;
	}


	public void setStopID(int stopID) {
		this.stopID = stopID;
	}


	public String getSequence() {
		return sequence;
	}


	public void setSequence(String sequence) {
		this.sequence = sequence;
	}


	public String getHeadsign() {
		return headsign;
	}


	public void setHeadsign(String headsign) {
		this.headsign = headsign;
	}


	public String getPickUpType() {
		return pickUpType;
	}


	public void setPickUpType(String pickUpType) {
		this.pickUpType = pickUpType;
	}


	public String getDropoffType() {
		return dropoffType;
	}


	public void setDropoffType(String dropoffType) {
		this.dropoffType = dropoffType;
	}
	
}
