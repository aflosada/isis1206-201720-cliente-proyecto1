package model.vo;

public class VOCalendar <T> 
{
	String service_id;
	int monday, tuesday, wednesday, thursday, friday, saturday, sunday;
	String start_date, end_date;
	
	public VOCalendar(String pservice_id, int pmonday,int ptuesday, int pwednesday,int pthursday,int pfriday,int psaturday,int psunday,String pstart_date,String pend_date)
	{
		service_id = pservice_id;
		monday = pmonday;
		tuesday = ptuesday;
		wednesday = pwednesday;
		thursday = pthursday;
		friday = pfriday;
		saturday = psaturday;
		sunday = psunday;
		
	}

	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public int getMonday() {
		return monday;
	}

	public void setMonday(int monday) {
		this.monday = monday;
	}

	public int getTuesday() {
		return tuesday;
	}

	public void setTuesday(int tuesday) {
		this.tuesday = tuesday;
	}

	public int getWednesday() {
		return wednesday;
	}

	public void setWednesday(int wednesday) {
		this.wednesday = wednesday;
	}

	public int getThursday() {
		return thursday;
	}

	public void setThursday(int thursday) {
		this.thursday = thursday;
	}

	public int getFriday() {
		return friday;
	}

	public void setFriday(int friday) {
		this.friday = friday;
	}

	public int getSaturday() {
		return saturday;
	}

	public void setSaturday(int saturday) {
		this.saturday = saturday;
	}

	public int getSunday() {
		return sunday;
	}

	public void setSunday(int sunday) {
		this.sunday = sunday;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean hasDate(String fecha) 
	{
		boolean rta = false;
		// TODO Auto-generated method stub
		int intervaloI = Integer.parseInt(start_date.substring(0, 3));
		int intervaloIFecha = Integer.parseInt(fecha.substring(0, 3));
		
		int intervaloM = Integer.parseInt(start_date.substring(4, 5));
		int intervaloMFecha = Integer.parseInt(fecha.substring(4, 5));
		
		int intervaloF = Integer.parseInt(start_date.substring(6, 7));
		int intervaloFFecha = Integer.parseInt(fecha.substring(6, 7));
		
		if(intervaloI <= intervaloIFecha && intervaloM <= intervaloMFecha && intervaloF <= intervaloFFecha )
		{
			rta = true;
		}
		return rta;
	}
	
	
	
	
}