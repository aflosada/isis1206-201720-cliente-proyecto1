package model.vo;

public class VOSchedule {

	private String pattern;
	private String Destination;
	private String expectedTimeLeave;
	private String expectedCountdown;
	private String scheduleStatus;
	private String cancelledTrip;
	private String cancelledStop;
	private String addedTrip;
	private String addedStop;
	private String lastUpdate;
	
	
	public VOSchedule(String pattern, String destination, String expectedTimeLeave, String expectedCountdown,
			String scheduleStatus, String cancelledTrip, String cancelledStop, String addedTrip, String addedStop,
			String lastUpdate) {
		super();
		this.pattern = pattern;
		Destination = destination;
		this.expectedTimeLeave = expectedTimeLeave;
		this.expectedCountdown = expectedCountdown;
		this.scheduleStatus = scheduleStatus;
		this.cancelledTrip = cancelledTrip;
		this.cancelledStop = cancelledStop;
		this.addedTrip = addedTrip;
		this.addedStop = addedStop;
		this.lastUpdate = lastUpdate;
	}


	public String getPattern() {
		return pattern;
	}


	public void setPattern(String pattern) {
		this.pattern = pattern;
	}


	public String getDestination() {
		return Destination;
	}


	public void setDestination(String destination) {
		Destination = destination;
	}


	public String getExpectedTimeLeave() {
		return expectedTimeLeave;
	}


	public void setExpectedTimeLeave(String expectedTimeLeave) {
		this.expectedTimeLeave = expectedTimeLeave;
	}


	public String getExpectedCountdown() {
		return expectedCountdown;
	}


	public void setExpectedCountdown(String expectedCountdown) {
		this.expectedCountdown = expectedCountdown;
	}


	public String getScheduleStatus() {
		return scheduleStatus;
	}


	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}


	public String getCancelledTrip() {
		return cancelledTrip;
	}


	public void setCancelledTrip(String cancelledTrip) {
		this.cancelledTrip = cancelledTrip;
	}


	public String getCancelledStop() {
		return cancelledStop;
	}


	public void setCancelledStop(String cancelledStop) {
		this.cancelledStop = cancelledStop;
	}


	public String getAddedTrip() {
		return addedTrip;
	}


	public void setAddedTrip(String addedTrip) {
		this.addedTrip = addedTrip;
	}


	public String getAddedStop() {
		return addedStop;
	}


	public void setAddedStop(String addedStop) {
		this.addedStop = addedStop;
	}


	public String getLastUpdate() {
		return lastUpdate;
	}


	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


	@Override
	public String toString() {
		return "VOSchedule [pattern=" + pattern + ", Destination=" + Destination + ", expectedTimeLeave="
				+ expectedTimeLeave + ", expectedCountdown=" + expectedCountdown + ", scheduleStatus=" + scheduleStatus
				+ ", cancelledTrip=" + cancelledTrip + ", cancelledStop=" + cancelledStop + ", addedTrip=" + addedTrip
				+ ", addedStop=" + addedStop + ", lastUpdate=" + lastUpdate + "]";
	}
	
	


	
	
	
}
