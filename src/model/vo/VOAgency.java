package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOAgency <T>
{
	String agency_id;
	String agency_name;
	String agency_url;
	String agency_timezone;
	String agency_lang;
	private DoubleLinkedList<VORoute> listaRutas;
	



	public VOAgency(String agencyId, String an, String au, String at, String al)
	{
		agency_id = agencyId;
		agency_name = an;
		agency_url = au;
		agency_timezone = at;
		agency_lang = al;
		listaRutas = new DoubleLinkedList<VORoute>();
	}
	
	
	public DoubleLinkedList<VORoute> getListaRutas() {
		return listaRutas;
	}
	
	public String getAgency_id() 
	{
		return agency_id;
	}

	public void setAgency_id(String agency_id) 
	{
		this.agency_id = agency_id;
	}

	public String getAgency_name() 
	{
		return agency_name;
	}

	public void setAgency_name(String agency_name) 
	{
		this.agency_name = agency_name;
	}

	public String getAgency_url() 
	{
		return agency_url;
	}

	public void setAgency_url(String agency_url) 
	{
		this.agency_url = agency_url;
	}

	public String getAgency_timezone() 
	{
		return agency_timezone;
	}

	public void setAgency_timezone(String agency_timezone) 
	{
		this.agency_timezone = agency_timezone;
	}

	public String getAgency_lang() 
	{
		return agency_lang;
	}

	public void setAgency_lang(String agency_lang)
	{
		this.agency_lang = agency_lang;
	}

	
}
