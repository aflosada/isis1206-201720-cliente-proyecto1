package API;

import java.io.File;

import model.data_structures.ILista;
import model.data_structures.IStack;
import model.vo.RangoHora;
import model.vo.Retardo;
import model.vo.VORetardo;
import model.vo.VORoute;
import model.vo.VOService;
import model.vo.VOStop;
import model.vo.VOTransfers;
import model.vo.VOTrips;

public interface ISTSManager 
{
	
	/**
	 * Carga el archivo agency.csv
	 * @param agencyFile
	 */
	
	public void cargarAgency(String agencyFile);
	
	/**
	 * Carga el archivo stops.csv
	 * @param stopsFile
	 */
	
	public void cargarStops(String stopsFile);
	
	/**
	 * Carga el archivo trips
	 * @param tripsFile
	 */
	
	public void cargarTrips(String tripsFile);
	
	/**
	 * Carga el archivo Calendar.csv
	 * @param calendarFile
	 */
	
	public void cargarCalendar(String calendarFile);
	
	/**
	 * Cargar el archivo CalendarDates.csv
	 * @param calendarDatesFile
	 */
	
	public void cargarCalendarDates(String calendarDatesFile);
	
	/**
	 * Carga el archivo routes.csv
	 * @param routesFile
	 */
	
	public void cargarRoutes(String routesFile);
	
	/**
	 * Carga el archivo shapes.csv
	 * @param shapesFile
	 */
	
	public void cargarShapes(String shapesFile);
	
	/**
	 * Carga el archivo feed_Info.csv
	 * @param feedInfoFile
	 */
	
	public void cargarFeedInfo(String feedInfoFile);
	
	/**
	 * Carga el archivo StopTimes.csv
	 * @param stopTimesFile
	 */
	
	public void cargarStopTimes(String stopTimesFile);
	
	/**
	 * Carga el archivo Transfers.csv
	 * @param transfersFile
	 */
	
	public void cargarTransfers(String transfersFile);
	
	/**
	 * Cargar los archivos json de busservice
	 * @param rtFile
	 */
	
	public void cargarBusService(File rtFile);
	
	/**
	 * Carga los archivos json de StopEstimService
	 * @param rtFile
	 */
	
	public void cargarStopEstimService(File rtFile);
	
	
	/**
	 * Carga toda la informaci�n est�tica necesaria para la	
	 * operaci�n del sistema.		
	 * (Archivo de rutas, viajes, paradas, etc.)
	 */
	public void	ITScargarGTFS();
	
	/**
	 * Carga	la	informaci�n	en	tiempo	real	de	los	buses	en	para	fecha	determinada.		
	 * @param fecha
	 */	
	public void ITScargarTR(String fecha);
	
	/**
	 * Retorna	una	lista	de	rutas	que	son	prestadas	por	una	empresa	determinada,	en	
	   una	fecha	determinada
	 * @param nombreEmpresa
	 * @param fecha
	 * @return
	 */	
	public ILista<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha);
	
	/**
	 * Retorna	una	lista	de	viajes	retrasados	para	una	ruta	en	una	fecha	determinada
	 * @param idRuta
	 * @param fecha
	 * @return
	 */	
	public ILista<VOTrips> ITSviajesRetrasadosRuta(String idRuta, String fecha);
	
	/**
	 * Retorna	una	lista	de	Paradas	en	las	que	hubo	un	retraso	en	una	fecha	dada
	 * @param fecha
	 * @return
	 */	
	public ILista<VOStop> ITSparadasRetrasadasFecha( String fecha);
	
	/**
	 * Retorna	una	lista	de	paradas,	las	cuales	conforman	todos	los	posibles	transbordos	
	   que	se	derivan	a	partir	de	la	primera	parada	de	dicha	ruta.
	 * @param idRuta
	 * @param fecha 
	 * @return
	 */	
	public ILista<VOTransfers> ITStransbordosRuta(String idRuta, String fecha);
	
	/**
	 * Retorna	una	lista	en	la	que	en	cada	posici�n	se	tiene	una	lista	con	la	informaci�n	de	
	una	parada	espec�fica.		A	su	vez	en	cada	posici�n	de	esta	lista,	se	tiene	una	lista	con	
	los	viajes		de	la	parada	que	se	est�	analizando	(Punto	5A)
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @return
	 */	
	public ILista <ILista<ILista<VOStop>>> ITSrutasPlanUtilizacion (String fecha, String	horaInicio, String horaFin);
		
	/**
	 * Retorna	una	lista	de	rutas	que	son	prestadas	por	una	empresa	determinada,	en	
	una	fecha	determinada.	La	lista	est�	ordenada	por	cantidad	total	de	paradas.
	 * @param idRuta
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */	
	public ILista<VOTrips> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) throws Exception;
	
	/**
	 * Retorna	una	lista	ordenada	con	rangos	de	hora	en	los	que	mas	retardos	
	hubo.
	 * @param idRuta
	 * @param Fecha
	 * @return
	 */	
	public ILista<RangoHora> ITSretardoHoraRuta (String idRuta, String Fecha );
	
	/**
	 * Retorna	una	lista	con	los	viajes	para	ir	de	la	parada	de	inicio	a	la	parada	
	final	en	una	fecha	y	franja	horaria	determinada.
	 * @param idOrigen
	 * @param idDestino
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @return
	 * @throws Exception 
	 */	
	public ILista <VOTrips> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin) throws Exception;
	
	/**
	 * Retorna la ruta que tiene menos retardo promedio en distancia de sus viajes
	en una fecha dada. El retardo promedio en distancia de un viaje corresponde
	al total de los tiempos de retardo de sus paradas dividido por la distancia
	recorrida.
	 * @param idRuta
	 * @return
	 */	
	public VORoute ITSrutaMenorRetardo (String idRuta);
	
	/**
	 * Inicializa	las	estructuras	de	datos	necesarias
	 */	
	public void ITSInit();
	
	/**
	 * Retorna	una	lista	con	los	servicios	que	mas	distancia	recorren	en	una	
	fecha.		La	lista	est�	ordenada	por	distancia.
	 * @param fecha
	 * @return
	 */	
	public ILista<VOService> ITSserviciosMayorDistancia (String fecha);
	
	/**
	 * Retorna	una	lista	con	todos	los	retardos	de	un	viaje	en	una	fecha	dada.
	 * @param fecha
	 * @param idViaje
	 * @return
	 */	
	public ILista<Retardo> ITSretardosViaje (String fecha, String idViaje);
	
	/**
	 * Retorna	una	lista	con	todas	las	paradas	del	sistema	que	son	compartidas	
	por	mas	de	una	ruta	en	una	fecha	determinada
	 * @param fecha
	 * @return
	 */	
	public ILista <VOStop> ITSparadasCompartidas (String fecha);

	VORoute ITSrutaMenorRetardo(String idRuta, String fecha);

	public ILista<VORoute> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha);
	
}
