package controller;

import API.ISTSManager;
import model.data_structures.*;
import model.exceptions.DateNotFoundExpection;
import model.logic.STSManager;
import model.vo.*;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ISTSManager manager  = new STSManager();

	/**
	 * inicializa estructuras
	 */
	public static void ITSInit() 
	{
		manager.ITSInit();	
	}

	/**
	 * C1
	 */
	public static void ITScargarGTFS() 
	{
		manager.ITScargarGTFS();	
	}

	/**
	 * C1
	 * @param fecha
	 */
	public static void ITScargarTR(String fecha) throws DateNotFoundExpection
	{
		manager.ITScargarTR(fecha);
	}

	/**
	 * A1
	 */
	public static ILista<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha)
	{
		return manager.ITSrutasPorEmpresa(nombreEmpresa, fecha);
	}

	/**
	 * A2
	 */
	public static ILista<VOTrips> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		return manager.ITSviajesRetrasadosRuta(idRuta, fecha);
	}

	/**
	 * A3
	 */
	public static ILista<VOStop> ITSparadasRetrasadasFecha(String fecha) 
	{
		return manager.ITSparadasRetrasadasFecha(fecha);
	}

	/**
	 * A4
	 */
	public static ILista<VOTransfers> ITStransbordosRuta(String idRuta, String fecha)
	{
		return manager.ITStransbordosRuta(idRuta, fecha);
	}

	/**
	 * A5
	 */
	public static VOPlan ITSrutasPlanUtilizacion(ILista<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{	
		return manager.ITSrutasPlanUtilizacion(idsDeParadas, fecha, horaInicio, horaFin);
	}

	/**
	 * B1
	 */
	public static ILista<VORoute> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)
	{
		return manager.ITSrutasPorEmpresaParadas(nombreEmpresa, fecha);
	}

	/**
	 * B2
	 * @throws Exception 
	 */
	public static ILista<VOTrips> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) throws Exception
	{	
		return manager.ITSviajesRetrasoTotalRuta(idRuta, fecha);
	}

	/**
	 * B3
	 */
	public static ILista<RangoHora> ITSretardoHoraRuta(String idRuta, String Fecha)
	{
		return manager.ITSretardoHoraRuta(idRuta, Fecha);
	}

	/**
	 * B4
	 * @throws Exception 
	 */
	public static ILista<VOTrips> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin) throws Exception 
	{ 		
		return manager.ITSbuscarViajesParadas(idOrigen, idDestino, fecha, horaInicio, horaFin);
	}

	/**
	 * B5
	 */
	public static VORoute ITSrutaMenorRetardo(String fecha) 
	{ 		
		return manager.ITSrutaMenorRetardo(fecha);
	}

	/**
	 * C2
	 */
	public static ILista<VOService> ITSserviciosMayorDistancia(String fecha)
	{ 		
		return manager.ITSserviciosMayorDistancia(fecha);
	}

	/**
	 * C3
	 */
	public static ILista<Retardo> ITSretardosViaje(String fecha, String idViaje) 
	{		
		return manager.ITSretardosViaje(fecha, idViaje);
	}

	/**
	 * C4
	 */
	public static ILista<VOStop> ITSparadasCompartidas(String fecha)
	{		
		return manager.ITSparadasCompartidas(fecha);
	}

}
