package view;

import java.util.Scanner;
import controller.Controller;
import model.data_structures.*;
import model.exceptions.DateNotFoundExpection;
import model.vo.*;

public class STSManagerView {

	/**
	 * Main
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while (!fin) {
			printMenu();

			int option = sc.nextInt();

			switch (option) {
			// 1C cargar data est�tica
			case 1:

				// Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				// Cargar data
				Controller.ITScargarGTFS();

				// Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime) / (1000000);

				// Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "
						+ ((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + " MB");

				break;

			// 1C cargar data tiempo real de bus para una fecha
			case 2:
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");

				// Fecha deseada
				String fechaCase2 = sc.next();

				// Cargar bus
				try {
					Controller.ITScargarTR(fechaCase2);
					System.out.println("Se cargo exit�samente la infomaci�n del bus para la feha " + fechaCase2);
				} catch (DateNotFoundExpection e) {
					System.out.println(e.getMessage());
				}

				break;
			// 1A
			case 3:

				// Nombre empresa
				System.out.println("Ingrese el nombre de la empresa");
				String empresaCase3 = sc.next();

				// Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase3 = sc.next();

				// Rutas por empresa
				ILista<VORoute> rutasPorEmpresa = Controller.ITSrutasPorEmpresa(empresaCase3, fechaCase3);
				System.out.println("Las rutas que son prestadas por la empresa " + empresaCase3 + " en la fecha "
						+ fechaCase3 + " son:\n");
				for (VORoute rutaCase3 : rutasPorEmpresa) 
				{
					System.out.println("Id: " + rutaCase3.getRouteId() + " | Short name: " + rutaCase3.getRouteSName());
				}
				System.out.println("\n");

				break;

			// 2A
			case 4:

				// id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase4 = sc.next();

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase4 = sc.next();

				// Viajes retrasados
				ILista<VOTrips> listaCase4 = Controller.ITSviajesRetrasadosRuta(idRouteCase4, fechaCase4);
				System.out.println("Los viajes que estan retrasados en la ruta con id " + idRouteCase4 + " en la fecha "
						+ fechaCase4 + "son:\n");
				for (VOTrips viajeCase4 : listaCase4) {
					System.out.println("Trip id: " + viajeCase4.getTripID());
				}
				System.out.println("\n");
				break;

//			// 3A
			case 5:

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase5 = sc.next();

				// paradas retrasadas
				ILista<VOStop> listaCase5 = Controller.ITSparadasRetrasadasFecha(fechaCase5);
				System.out.println("Las paradas en las que hubo retrasos en la fecha " + fechaCase5 + "son:\n");
				for (VOStop paradaCase5 : listaCase5) {
					System.out.println("Stop id:" + paradaCase5.getStopId() + "| N�mero de incidentes: "
							+ paradaCase5.getNumeroIncidentes());
				}
				System.out.println("\n");
				break;

//			// 4A
			case 6:

				// Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase6 = sc.next();

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase6 = sc.next();

				// Transbordos
				ILista<VOTransfers> listaCase6 = Controller.ITStransbordosRuta(idRutaCase6, fechaCase6);
				System.out.println("Hay " + listaCase6.getSize() + " posibles transbordos:" + "para la ruta con id "
						+ idRutaCase6 + " en la fecha " + fechaCase6 + "son:\n");
				int contadorCase6 = 1;
				for (VOTransfers transbordoCase6 : listaCase6) {
					System.out.println(
							"\n" + contadorCase6 + ". Tiempo en transbordo: " + transbordoCase6.getTransferTime());
					System.out.println("Paradas asociadas a transbordo:");

					// Lista de paradas en transbordo
					ILista<VOStop> listaParadasCase6 = transbordoCase6.getListadeParadas();
					for (VOStop paradaCase6 : listaParadasCase6) {
						System.out.println("   - Stop id:" + paradaCase6.getStopId() + "| N�mero de incidentes: "
								+ paradaCase6.getNumeroIncidentes());
					}
				}
				System.out.println("\n");
				break;

			// 5A
			case 7:

				// Secuencia de paradas
				System.out.println("Ingrese los ID's de paradas Ej: 643-381-1274 (separelos con guiones)");
				String idsCase7 = sc.next();
				String[] splited = idsCase7.split("-");

				// TODO inicialize y a�ada los id�s a la lista de secuencia de
				// paradas
				ILista<String> secuenciaDeParadas = null;

				// Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase7 = sc.next();

				// String hora inicio
				System.out.println("Ingrese la hora de inicio");
				String horaInicioCase7 = sc.next();

				// String hora fin
				System.out.println("Ingrese la hora de finalizaci�n");
				String horaFinCase7 = sc.next();

				VOPlan plan = Controller.ITSrutasPlanUtilizacion(secuenciaDeParadas, fechaCase7, horaInicioCase7,
						horaFinCase7);
				System.out.println("Las paradas en la fecha y rango dadas son: \n");
				for (VOPlan.ParadaPlanVO parada : plan.getSecuenciaDeParadas()) {
					// Parada
					System.out.println("Id parada:" + parada.getIdParada());
					System.out.println("Las rutas que utilizan esta parada son:");
					for (VOPlan.ParadaPlanVO.RutaPlanVO ruta : parada.getRutasAsociadasAParada()) {
						// Ruta en parada
						System.out.println("- Ruta id:" + ruta.getIdRuta());
						System.out.println("Los viajes en esta ruta asociados a la parada son:");
						for (VOPlan.ParadaPlanVO.RutaPlanVO.ViajePlanVO viaje : ruta.getViajesEnRuta()) {
							// viaje en ruta
							System.out.println(
									"     -" + viaje.getIdViaje() + "| Hora de parada :" + viaje.getHoraDeParada());
						}
					}
				}
				break;

			// 1B
			case 8:

				// empresa
				System.out.println("Ingrese el nombre de la empresa.");
				String empresaCase8 = sc.next();
				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase8 = sc.next();

				ILista<VORoute> listaRutas = Controller.ITSrutasPorEmpresaParadas(empresaCase8, fechaCase8);
				System.out.println("Existen " + listaRutas.getSize() + " rutas que cumplen las condiciones dadas.");
				for (VORoute rutaCase8 : listaRutas) {
					System.out.println("- Id de la ruta: " + rutaCase8.getRouteId());
					System.out.println("- Nombre de la ruta: " + rutaCase8.getRouteSName());

				}

				break;

			// 2B
			case 9:

				// Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase9 = sc.next();

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase9 = sc.next();

				ILista<VOTrips> listaViajes = Controller.ITSviajesRetrasoTotalRuta(idRutaCase9, fechaCase9);
				System.out.println("Existen " + listaViajes.getSize() + " rutas que cumplen las condiciones dadas.");
				for (VOTrips viajeCase8 : listaViajes) {
					System.out.println("Id del viaje: " + viajeCase8.getTripID());
				}
				break;

			// 3B
			case 10:

				// Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase10 = sc.next();

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase10 = sc.next();

				RangoHora rangoHora10 = Controller.ITSretardoHoraRuta(idRutaCase10, fechaCase10);
				System.out.println("Informaci�n del rango:");
				System.out.println("Hora inicial: " + rangoHora10.getHoraInicial());
				System.out.println("Hora final: " + rangoHora10.getHoraFinal());
				System.out.println("N�mero de retardos: " + rangoHora10.getListaRetardos().Size());

				// 4B
			case 11:
				// Id ruta origen
				System.out.println("Ingrese el id de la parada origen");
				String idOrigen11 = sc.next();
				// Id ruta destino
				System.out.println("Ingrese el id de la parada destino");
				String idDestino11 = sc.next();
				// Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase11 = sc.next();
				// Hora de inicio
				System.out.println("Ingrese la hora inicial");
				String horaInicio11 = sc.next();
				// Hora de inicio
				System.out.println("Ingrese la hora final");
				String horaFin11 = sc.next();

				ILista<VOTrips> listaViajes11 = Controller.ITSbuscarViajesParadas(idOrigen11, idDestino11, fechaCase11,
						horaInicio11, horaFin11);
				System.out.println("Existen " + listaViajes11.getSize() + " rangos que cumplen las condiciones dadas.");
				for (VOTrips viaje11 : listaViajes11) {

				}
				break;

			// 5B
			case 12:
				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase12 = sc.next();

				VORoute ruta12 = Controller.ITSrutaMenorRetardo(fechaCase12);
				System.out.println("Id de la ruta: " + ruta12.getRouteId());
				System.out.println("Nombre corto de la ruta: " + ruta12.getRouteSName());
				System.out.println("Retardo promedio de la ruta: ");

				break;

			// 2C
			case 13:

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase13 = sc.next();

				ILista<VOService> listaCase13 = Controller.ITSserviciosMayorDistancia(fechaCase13);
				System.out.println("Los servicios que mas distancia corren en la fecha " + fechaCase13 + " son:\n");
				for (VOService servicioVO : listaCase13) {
					System.out.println("Id del servicio: " + servicioVO.toString() + "| Distancia recorrida: "
							+ servicioVO.getDistanciaRecorrida());
				}
				break;
			// 3C
			case 14:
				// Id viaje
				System.out.println("Ingrese el id del viaje deseado");
				String idViajeCase14 = sc.next();

				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase14 = sc.next();

				ILista<Retardo> listaRetardos14 = Controller.ITSretardosViaje(fechaCase14, idViajeCase14);
				System.out
						.println("Existen " + listaRetardos14.getSize() + " rangos que cumplen las condiciones dadas.");
				for (Retardo retar14 : listaRetardos14) {
					System.out.println(retar14.toString());
				}

				// 4C
			case 15:
				// fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase15 = sc.next();

				ILista<VOStop> listaParadas15 = Controller.ITSparadasCompartidas(fechaCase15);
				System.out
						.println("Existen " + listaParadas15.getSize() + " paradas que cumplen las condiciones dadas.");
				for (VOStop parada15 : listaParadas15) {
					System.out.println("Id de la parada: " + parada15.getStopId());
					System.out.println("Numero de incidentes: " + parada15.getNumeroIncidentes());
				}

				// Salir
			case 16:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	/**
	 * Menu
	 */
	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar la informaci�n est�tica necesaria para la operaci�n del sistema");
		System.out.println("2. Cargar la informaci�n en tiempo real de los buses dada una fecha\n");

		System.out.println("Parte A:");
		System.out.println(
				"3.Obtenga una lista de rutas ordenada por id, que son ofrecidas por una empresa determinada en una fecha dada. (1A)");
		System.out.println("4.Dada una ruta y una fecha, obtenga los viajes retrasados ordenados por id. (2A)");
		System.out.println(
				"5.Obtenga una lista de Paradas ordenada por el numero de total incidentes, en las que hubo un retraso en una fecha dada. (3A)");
		System.out.println(
				"6.Obtenga una lista ordenada (por tiempo total de viaje) con todos los transbordos posibles y las paradas asociadas a dichos transbordos para una determinada ruta en una fecha dada(4A)");
		System.out.println(
				"7.Obtenga una lista en la que en cada posici�n se tiene una lista con la informaci�n de una parada espec�fica. "
						+ "\n  A su vez en cada posici�n de esta lista, se tiene una lista con los viajes de la parada que se est� analizando. (5A)\n");

		System.out.println("Parte B: ");
		System.out.println(
				"8. Dadas una empresa y una fecha, obtenga una lista ordenada (por cantidad de paradas) con las rutas cubiertas que cubre la empresa en la fecha.(1B)");
		System.out.println(
				"9. Dada una ruta y una flecha, obtenga una lista ordenada (por id de viaje y localizaci�n de las paradas de mayor a menor tiempo de retardo)\n  de los viajes que despu�s de un retardo tuvieron retardo en todas las siguientes paradas. (2B)");
		System.out.println(
				"10. Dada una ruta y una fecha dada, obtenga una lista ordenada (por tiempo total de retardo) de horas de m�s retardos (3B)");
		System.out.println(
				"11. Dados el id de una parada de origen y una parada de destino, una fecha y franja de horario, obtener los viajes de bus que llegan de la parada origen a la destino sin transbordos en la fecha y horas dadas (4B)");
		System.out.println("12. Obtener la ruta que tiene el menor retardo promedio en una fecha dada (5B)\n");

		System.out.println("Parte C:");
		System.out.println(
				"13. Obtenga una lista con los servicios en una fecha ordenados por la distancia que recorren. (2C)");
		System.out.println("14. Obtenga una lista con todos los retardos de un viaje en una fecha dada. (3C)");
		System.out.println(
				"15. Obtenga una lista con todas las paradas del sistema que son compartidas por mas de una ruta en una fecha determinada.\n ");
		System.out.println("16. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}